//
//  KClientApi.swift
//  WebService
//
//  Created by Gagan on 18/02/15.
//  Copyright (c) 2015 Gagan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
//import AFNetworking

class rClientApi: NSObject {
    
    class var sharedInstance: rClientApi {
        struct Static {
            static var instance: rClientApi?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = rClientApi()
        }
        
        return Static.instance!
    }
    
    override init(){
        super.init()
        
    }
    
    deinit
    {
        
    }
    
    
    func getJson(path : String , withParameters  paramerters : [String : String] , success : (AnyObject?) -> () , failure : (NSError) -> () )
    {
        
//        let manager = AFHTTPRequestOperationManager()
//        manager.GET(
//            "http://headers.jsontest.com",
//            parameters: nil,
//            success: { (operation: AFHTTPRequestOperation!,
//                responseObject: AnyObject!) in
//            },
//            failure: { (operation: AFHTTPRequestOperation!,
//                error: NSError!) in
//            }
//        )
        
        
    }
    
    
    //MARK: - Post Request
    
    func postRequest(path : String , withParameters  paramerters : [String : String] , success : (AnyObject?) -> () , failure : (NSError) -> () )
    {
        let completePath = path
        Alamofire.request(.POST, completePath, parameters: paramerters , encoding: ParameterEncoding.URL, headers: nil).responseJSON { response in
            switch response.result {
            case .Success(let data):
                success(data)
            case .Failure(let error):
                failure(error)
            }
        }
    }
    
    
    
    //MARK: - Get Request
    func postPath(path : String , withParameters  paramerters : [String : String] , success : (AnyObject?) -> () , failure : (NSError) -> () )
    {
        
        let url = path
        Alamofire.request(.GET, url).responseJSON { response in
            switch response.result {
            case .Success(let data):
                success(data)
            case .Failure(let error):
                failure(error)
            }
        }
    }
    
    
    func postFileWithParameters (path : String , paramerters : [String : String] , profilePic : UIImage? , success : (AnyObject?) -> () , failure : (NSError) -> ()){
        
        let manager = AFHTTPRequestOperationManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.POST(path, parameters: paramerters , constructingBodyWithBlock: { (formData) -> Void in
            
            if let ppic = profilePic,data = UIImageJPEGRepresentation(ppic, 1.0){
                
                formData.appendPartWithFileData(data, name:"image" , fileName:"temp.jpeg" , mimeType: "image/jpeg")
            }
            
            }, success: { (operation, response) -> Void in
                
                do{
                    success( try NSJSONSerialization.JSONObjectWithData(response as! NSData, options: .MutableLeaves))
                }catch{
                    
                }
                
                
            }) { (operation, error) -> Void in
                failure(error)
                
        }
    }
    
    func postFileWithParameters (path : String , paramerters : [String : String] , images : [UIImage]? , success : (AnyObject?) -> () , failure : (NSError) -> ()){
        
        let manager = AFHTTPRequestOperationManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        
        manager.POST(path, parameters: paramerters , constructingBodyWithBlock: { (formData) -> Void in
            if let arrayImages = images{
                for (_, image) in arrayImages.enumerate() {
                    guard let data = UIImageJPEGRepresentation(image as UIImage, 0.0) else{
                        break
                    }
                    formData.appendPartWithFileData(data, name:"media"   , fileName: "\(arc4random())" + "neoD1.jpeg"   , mimeType: "image/jpeg")
                }
            }
            },
            success: { (operation, response) -> Void in
                
                do{
                    success( try NSJSONSerialization.JSONObjectWithData(response as! NSData, options: .MutableLeaves))
                }catch{
                    
                }
                
            }) { (operation, error) -> Void in
                failure(error)
        }
    }

    
}
