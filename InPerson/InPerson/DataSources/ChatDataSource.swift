//
//  ChatDataSource.swift
//  InPerson
//
//  Created by CB Macmini_3 on 10/01/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

class ChatDataSource: NSObject,UITableViewDataSource,UITableViewDelegate {

    var messages : [Message]?
    
    var tableView  : UITableView?
    var shouldAnimateCell : Bool = false
    
    var tableViewRowHeight : CGFloat = 44.0
    
    typealias  TableViewCellConfigureBlock = (cell : AnyObject , item : AnyObject,previousItem : AnyObject,indexPath : NSIndexPath) -> ()
    typealias  DidSelectedRow = (indexPath :NSIndexPath) -> ()
    
    var configureCellBlock : TableViewCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?
    
    init(items : [Message],tableView : UITableView,configureCellBlock : TableViewCellConfigureBlock , aRowSelectedListener : DidSelectedRow) {
        super.init()
        
        self.tableView = tableView
        self.messages = items
        self.configureCellBlock = configureCellBlock
        self.aRowSelectedListener = aRowSelectedListener
        
    }
    
    override init() {
        
        super.init()
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages?.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier(self.getIdentifier(indexPath: indexPath) , forIndexPath: indexPath) as UITableViewCell
        
        if let block = self.configureCellBlock , item: AnyObject = self.messages?[indexPath.row],prevItem : AnyObject = indexPath.row == 0 ? self.messages?[indexPath.row] : self.messages?[indexPath.row - 1] {
            block(cell: cell , item: item,previousItem: prevItem,indexPath: indexPath)
        }
        return cell
    }
    
    var my_id = RKUserSingleton.sharedInstance.loggedInUser?.userId ?? "0"
    private func getIdentifier (indexPath indexPath : NSIndexPath) -> String{
        let identifier = CellIdentifiers.MyTextCell
        if let list = messages, _ = list[indexPath.row].messageImage {
            return CellIdentifiers.MyImageCell
        }
        guard let list = messages ,image = list[indexPath.row].messageMedia, user_id = list[indexPath.row].messageSentBy   else{ return identifier }
        let type = image == "" ? CellType.TextCell : CellType.ImageCell
        switch  type  {
        case CellType.TextCell :
            return user_id == my_id ? CellIdentifiers.MyTextCell : CellIdentifiers.OTextCell
        case CellType.ImageCell :
            return user_id == my_id ? CellIdentifiers.MyImageCell : CellIdentifiers.OImageCell
        
        default : return user_id == my_id ? CellIdentifiers.MyTextCell : CellIdentifiers.OTextCell
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
       return UITableViewAutomaticDimension

    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let block = self.aRowSelectedListener{
            block(indexPath: indexPath)
        }
//        guard let cell = self.tableView?.cellForRowAtIndexPath(indexPath) as? UserChatCell,chatBubble = cell.viewWithTag(9260) as? UIImageView else { return }

    }

    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        guard let tempMessage = self.messages?[indexPath.row] else { return 0.0 }
        let identifier = self.getIdentifier(indexPath: indexPath)
        if identifier == CellIdentifiers.MyTextCell || identifier == CellIdentifiers.OTextCell{
            
            struct Static {
                static var sizingCell: UserChatCell?
            }
            if Static.sizingCell == nil {
                Static.sizingCell = self.tableView?.dequeueReusableCellWithIdentifier(identifier) as? UserChatCell
            }
            self.configureCell((Static.sizingCell)!, indexPath: indexPath, tempMessage: tempMessage)
            let size = Static.sizingCell?.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
            return (size?.height)! + 1.0
        }
        else{
            return 200
        }
    }
    
    func configureCell(cell :  UserChatCell , indexPath : NSIndexPath , tempMessage : Message){
        cell.currentMessage = tempMessage
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        
     self.tableView?.superview?.endEditing(true)
    }
}
