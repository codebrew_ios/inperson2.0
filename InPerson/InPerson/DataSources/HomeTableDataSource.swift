//
//  HomeTableDataSource.swift
//  InPerson
//
//  Created by CB Macmini_3 on 24/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation
//import MGSwipeTableCell
protocol MDDelegate {
    
    func showLinkedinProfile(indexpath : NSIndexPath)
}
class HomeTableDataSource: NSObject,UITableViewDataSource,UITableViewDelegate,SceneCellDelegate,MGSwipeTableCellDelegate,UserCellDelegate,InvitationAlertDelegate {
    
    var delegate : MDDelegate?
    var tableView  : UITableView?
    var view : UIView?
     
    
    var lastContentOffset : CGFloat = 0.0
    
    var invitationAlert : InvitationAlert!
    var headerView : HomeTableHeaderView?
    
    var labelPerson : UILabel?
    var btnSearchTop : UIButton?
    var viewNavbar : UIView?
    
    var homeVC : HomeViewController?
    
    var items : [User]?
    var contentCards : [ContentCard] = []
    
    func initWithTableView(tableView : UITableView?, view : UIView ,users : [User],headerView : HomeTableHeaderView,navBar : UIView,btnST : UIButton,labelp : UILabel) -> AnyObject {
        
        self.tableView = tableView
        self.view = view
        self.headerView = headerView
        
        self.labelPerson = labelp
        self.btnSearchTop = btnST
        self.viewNavbar = navBar
        self.tableView?.tableHeaderView = nil
        self.tableView?.addSubview(self.headerView!)
        items = users
        tableView?.reloadData()
        
        invitationAlert = InvitationAlert(frame: UIScreen.mainScreen().bounds)
        
        return self
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return items!.count + contentCards.count + 1 ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if items?.first?.userName == "messageBanner"{
                guard let cell = self.tableView?.dequeueReusableCellWithIdentifier("MessageBannerCell") as? MessageBannerCell else { return UITableViewCell() }
                cell.btnClose.addTarget(self, action: Selector("removeMessageBanner:"), forControlEvents: .TouchUpInside)
                return cell
            }else if let _ =  items?.first?.userId{
                guard let cell : UserCell = tableView.dequeueReusableCellWithIdentifier("UserCell") as? UserCell else { return UITableViewCell() }
                cell.currentUser = items?[indexPath.row]
                cell.delegate = self
                cell.indexPath = indexPath
                cell.delegatelp = self
                return cell
            }else{
                guard let cell : SceneCell = tableView.dequeueReusableCellWithIdentifier("SceneCell") as? SceneCell else { return UITableViewCell() }
                cell.indexPath = indexPath
                cell.delegate = self
                return cell
            }
        }else if indexPath.row == items!.count + contentCards.count{
            guard let cell = tableView.dequeueReusableCellWithIdentifier("RefreshCell") as? RefreshCell else { return UITableViewCell() }
            cell.btnRefresh.addTarget(self, action: Selector("refreshUsers:"), forControlEvents: .TouchUpInside)
            return cell
        }else if indexPath.row >= items!.count {
            
            guard let cell = self.tableView?.dequeueReusableCellWithIdentifier("ContentCardCell") as? ContentCardCell else { return UITableViewCell() }
            cell.currentContentCard = contentCards[indexPath.row - items!.count]
            return cell
        }else{
            
            if self.items?[indexPath.row].userName == "reqSent"{
                
                guard let cell = tableView.dequeueReusableCellWithIdentifier("RequestSentTableCell") as? RequestSentTableCell else { return UITableViewCell() }
                return cell
            }
            guard let _ = items?[indexPath.row].userId else {
                guard let cell : SceneCell = tableView.dequeueReusableCellWithIdentifier("SceneCell") as? SceneCell else { return UITableViewCell() }
                cell.indexPath = indexPath
                cell.delegate = self
                return cell
            }
            
            guard let cell : UserCell = tableView.dequeueReusableCellWithIdentifier("UserCell") as? UserCell else { return UITableViewCell() }
            cell.currentUser = items?[indexPath.row]
            cell.delegatelp = self
            
            cell.indexPath = indexPath
            guard let savedUserId = RKUserSingleton.sharedInstance.loggedInUser?.userId, userId = cell.currentUser where userId != savedUserId else { return cell }
            if cell.labelIndicator.text == "You   " { return cell }
            cell.rightButtons = [MGSwipeButton(title: "", icon: UIImage(named: "ic_Connect_btn.png"), backgroundColor: UIColor.clearColor())
            ]
            cell.delegate = self
            
            cell.rightSwipeSettings.transition = .Border
            return cell
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            if items?.first?.userName == "messageBanner"{
                
                return 56
            }else if let _ =  items?[0].userId{
                return 164
                
            }else{
                let width = UIScreen.mainScreen().bounds.size.width - 16
                
                return width * 9/16
                //  return 186
            }
        }else if indexPath.row == items!.count + contentCards.count {
            return 80
        } else if indexPath.row >= items?.count {
            return 180
        }
        return 164
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //        if indexPath.row == items?.count{ refreshUsers() }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.row >= items?.count {
            if indexPath.row == items!.count + contentCards.count { return }
            homeVC?.selectedIndexPath = indexPath
            guard let VC = homeVC?.storyboard?.instantiateViewControllerWithIdentifier("ContentDescriptionViewController") as? ContentDescriptionViewController else { return }
           VC.currentCard = contentCards[indexPath.row - items!.count]
//            VC.transitioningDelegate = transition
            homeVC?.presentViewController(VC, animated: true, completion: nil)
            return
        }
        
        if items?[indexPath.row].userName == "messageBanner" {
            guard let VC = homeVC?.storyboard?.instantiateViewControllerWithIdentifier("ChatViewController") as? ChatViewController else { return }
            VC.otherUserName = "Team InPerson"
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "IPMessageBanner")
            VC.otherUserId = "0"
            homeVC?.navigationController?.pushViewController(VC, animated: true)
        }
        
        guard let cell = self.tableView?.cellForRowAtIndexPath(indexPath) as? UserCell else { return }
        
        if cell.labelIndicator.text == "You      " { return  }else if let userconnected = cell.currentUser?.userConnected where userconnected == "1" { return  }else if cell.labelIndicator.text == "Was Here      " { return  }else {
            
            cell.showSwipe(MGSwipeDirection.RightToLeft, animated: true)
        }
        //        guard let cell = self.tableView?.cellForRowAtIndexPath(indexPath) as? UserCell else { return }
    }
    
    func refreshUsers(sender : UIButton){
        
        guard let userId = RKUserSingleton.sharedInstance.loggedInUser?.userId,lat = RKLocationManager.sharedInstance.currentLocation?.current_lat , lng = RKLocationManager.sharedInstance.currentLocation?.current_lng,token = RKUserSingleton.sharedInstance.loggedInUser?.userToken,currentAirport = RKUserSingleton.sharedInstance.selectedAirport,airId = currentAirport.airportId else {
            return }
        homeVC?.showLoader()
        homeVC?.getUsersWithoutAirport(userId, airId: airId, token: token, lat: lat, lng: lng)
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        lastContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let scrollOffset = -scrollView.contentOffset.y
        let yPos = scrollOffset - 300
        //        print(" \(scrollOffset) + \(yPos) ")
        btnSearchTop?.alpha = 1.0
        labelPerson?.alpha = 1.0
        //        let alpha = 1.0 - (-yPos/300)
        let progress = 1.0 - (-yPos/600)
        let newRed   = (1.0 - progress) * (170.0/255.0)   + progress * 1
        let newGreen = (1.0 - progress) * (170.0/255.0) + progress * 1
        let newBlue  = (1.0 - progress) * (170.0/255.0)  + progress * 1
        let color = UIColor(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
        self.headerView?.viewSearch.backgroundColor = color
        
        headerView?.btnSearchBlue.alpha = 1.0 - ((-yPos * 3.5)/300)
        if yPos <= 0 {
            headerView?.viewSearchHeight.constant = 44 - (-yPos * 0.05) < 0 ? 0 : 44 - (-yPos * 0.05)
        }
        
        let x = 20 + (0.3 * -yPos)
        //        let x2 = 20 + (0.1 * -yPos)
        let x3 = 20 + (0.5 * -yPos)
        //        print("\(x)  \(x2)  \(x3)")
        //        print(x + "  " + x2 + "  " + x3)
        
        if x >= 20 {
            self.headerView?.constraintTrailing.constant = x3 <= 104 ? x3 : 104
            self.headerView?.constraintLeadingViewSearch.constant = x <= 52 ? x : 52
        }else{
            self.headerView?.constraintTrailing.constant = 20
            self.headerView?.constraintLeadingViewSearch.constant = 20
            
        }
        if -yPos >= 216 {
            
            btnSearchTop?.hidden = false
            self.headerView?.viewSearch?.hidden = true
        }else {
            btnSearchTop?.hidden = true
            self.headerView?.viewSearch?.hidden = false
        }
        if -yPos >= 230 {
            
            
            self.viewNavbar?.backgroundColor = UIColor.whiteColor()
            
        }else {
            
            
            self.viewNavbar?.backgroundColor = UIColor.clearColor()
            //                self.labelPerson?.alpha = 1.0
        }
        //        if x >= 55 { self.labelPerson?.alpha = 0.0 } else { self.labelPerson?.alpha = 1.0 }
        
        //
        //        btnSearch?.frame = CGRectMake(0, 0, viewSearch!.frame.size.width, viewSearch!.frame.size.height)
        //        lastContentOffset = scrollView.contentOffset.y
        
        
        self.homeVC?.updateHeaderView()
    }
    
    func swipeTableCell(cell: MGSwipeTableCell!, tappedButtonAtIndex index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        
        let userCell = cell as! UserCell
        
        guard let otherUserId = userCell.currentUser?.userId else { return false }
        guard let indexpath = items?.indexOf(userCell.currentUser!) else { return false }
        
        //        invitationAlert.indexPath = NSIndexPath(forRow: indexpath, inSection: 0)
        if let h = RKUserSingleton.sharedInstance.loggedInUser?.userHide where h == "1"  {
            self.homeVC?.showNativeAlert("Please turn off HIDE ME to connect.")
            return false
        }
        
        connectToUser(otherUserId,indexPath: NSIndexPath(forRow: indexpath, inSection: 0))
        
        return true
    }
    
    func btnDismissCell(indexPath : NSIndexPath) {
        
        self.tableView?.beginUpdates()
        items?.removeAtIndex(indexPath.row)
        self.tableView?.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .None)
        self.tableView?.endUpdates()
        //        self.tableView?.reloadData()
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "IPShowcard")
    }
    
    func removeMessageBanner(sender : UIButton){
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "IPMessageBanner")
        self.tableView?.beginUpdates()
        items?.removeFirst()
        self.tableView?.deleteRowsAtIndexPaths([NSIndexPath(forRow: 0, inSection: 0)], withRowAnimation: .Fade)
        self.tableView?.endUpdates()
    }
    
    
    func connectToUser(otherUserId : String,indexPath : NSIndexPath){
        guard let user = RKUserSingleton.sharedInstance.loggedInUser else { return }
        var removedUser = User()
        self.tableView?.beginUpdates()
        removedUser = items![indexPath.row]
        self.items?.removeAtIndex(indexPath.row)
        self.tableView?.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
        self.tableView?.endUpdates()
        
        self.tableView?.beginUpdates()
        let user1 = User()
        user1.userName = "reqSent"
        self.items?.insert(user1, atIndex: indexPath.row)
        self.tableView?.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Right)
        self.tableView?.endUpdates()
        
        User.connectUser(user.userId!, otherUserId: otherUserId, success: { () -> () in
            //            guard let showAlert = NSUserDefaults.standardUserDefaults().valueForKey("showInvitationAlert") as? Bool else { return  }
            //            if !showAlert {
            //                self.showInvitationAlert()
            //            }else {
            //
            //            }
            
            
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2.0 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                
                self.tableView?.beginUpdates()
                self.items?.removeAtIndex(indexPath.row)
                self.tableView?.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
                self.tableView?.endUpdates()
            }
            self.homeVC?.hideLoader()
            }, failure: { (error) -> () in
                
                self.tableView?.beginUpdates()
                self.items?.removeAtIndex(indexPath.row)
                self.tableView?.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Right)
                self.tableView?.endUpdates()
                
                self.tableView?.beginUpdates()
                self.items?.insert(removedUser, atIndex: indexPath.row)
                self.tableView?.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Left)
                self.tableView?.endUpdates()
                
                
                
                
                
                guard let VC = self.homeVC else { return }
                VC.showNativeAlert(error)
            }) { () -> () in
                
        }
    }
    
    func swipeTableCell(cell: MGSwipeTableCell!, canSwipe direction: MGSwipeDirection, fromPoint point: CGPoint) -> Bool{
        let userCell = cell as! UserCell
        if userCell.labelIndicator.text == "You      " { return false }else if let userconnected = userCell.currentUser?.userConnected where userconnected == "1" { return false }else if userCell.labelIndicator.text == "Was Here      " { return false }
        
        return true
    }
    func showLinkedinProfile(indexPath: NSIndexPath) {
        
        self.delegate?.showLinkedinProfile(indexPath)
    }
    
    
    func showInvitationAlert(){
        
        guard let w = UIApplication.sharedApplication().delegate?.window,window = w else { return }
        invitationAlert.delegate = self
        window.addSubview(invitationAlert)
        window.bringSubviewToFront(invitationAlert)
    }
    
    func hideInvitationAlert(){
        
        self.invitationAlert.removeFromSuperview()
    }
    
    func okayPressed(indexPath: NSIndexPath?) {
        
        hideInvitationAlert()
    }
    
    func swipeTableCell(cell: MGSwipeTableCell!, shouldHideSwipeOnTap point: CGPoint) -> Bool {
        return true
    }
    
}
