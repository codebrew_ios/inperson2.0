//
//  MessageDataSource.swift
//  InPerson
//
//  Created by CB Macmini_3 on 08/01/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit
//import MGSwipeTableCell

class MessageDataSource: NSObject,UITableViewDataSource,UITableViewDelegate,RequestDelegate,MGSwipeTableCellDelegate,ChatUserCellDelegate,ProfileViewDelegate {

    var tableView : UITableView?
    var arrRequests : [Request]?
    var arrChats : [Chat]?
    var admMessage : Chat?
    
    var profileView = ProfileView(frame: UIScreen.mainScreen().bounds)
    
    var badgeView : LKBadgeView = LKBadgeView()
    
    var VC : MessageListingViewController?
    
    init(table : UITableView, req : [Request],chats : [Chat], selfVC : MessageListingViewController,adminMessage : Chat) {
            super.init()
        tableView = table
        arrRequests = req
        arrChats = chats
        VC = selfVC
        admMessage = adminMessage
        tableView?.reloadData()
        
        self.VC?.view.addSubview(profileView)
        profileView.alpha = 0.0
        profileView.delegate = self
    }
    
    override init() {
        super.init()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let countR = self.arrRequests?.count, countC = self.arrChats?.count else { return 0 }
        return countR + countC + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        guard let inviteCell = tableView.dequeueReusableCellWithIdentifier("InviteCell") as? InviteCell,chatCell = tableView.dequeueReusableCellWithIdentifier("ChatUserCell") as? ChatUserCell,req = self.arrRequests,chats = self.arrChats else { return UITableViewCell() }
        if let _ = admMessage?.chatId where indexPath.row == 0{
            chatCell.currentChat = admMessage
            chatCell.rightButtons = [MGSwipeButton(title: "", icon: UIImage(named: "ic_delete.png"), backgroundColor: UIColor(hex: 0xFF3C30))
            ]
            chatCell.delegate = self
            chatCell.chatDelegate = self
            chatCell.rightSwipeSettings.transition = .Drag
            return chatCell
        }else if indexPath.row - 1 < req.count {
            
            inviteCell.currentRequest = req[indexPath.row - 1]
            inviteCell.indexPath = indexPath
            inviteCell.delegate = self
            inviteCell.selectionStyle = .None
            return inviteCell
        }else {
            
            chatCell.currentChat = chats[indexPath.row  - (req.count + 1)]
            chatCell.rightButtons = [MGSwipeButton(title: "", icon: UIImage(named: "ic_delete.png"), backgroundColor: UIColor(hex: 0xFF3C30))
            ]
            chatCell.delegate = self
            chatCell.chatDelegate = self
            chatCell.rightSwipeSettings.transition = .Drag
            return chatCell
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        guard let req = self.arrRequests,_ = self.arrChats else { return }
        self.VC?.indexPath = indexPath
        if indexPath.row == 0{
            guard let chatVC = self.VC?.storyboard?.instantiateViewControllerWithIdentifier("ChatViewController") as? ChatViewController else { return }
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "IPMessageBanner")
            chatVC.userId2 = admMessage?.chatSentTo
            chatVC.myImageUrl = admMessage?.chatImage
            chatVC.otherUserName = admMessage?.chatName
            chatVC.placeholderHidden = admMessage?.chatMessage == "" ? false : true
            self.VC?.navigationController?.pushViewController(chatVC, animated: true)
        }else if indexPath.row < req.count {
            
            
        }else {
            
            guard let chatArr = arrChats ,reqArr = arrRequests,chatVC = self.VC?.storyboard?.instantiateViewControllerWithIdentifier("ChatViewController") as? ChatViewController else { return }
            let index = indexPath.row - (reqArr.count + 1)
            chatVC.userId2 = chatArr[index].chatSentTo
            chatVC.myImageUrl = chatArr[index].chatImage
            chatVC.otherUserName = chatArr[index].chatName
            chatVC.placeholderHidden = chatArr[index].chatMessage == "" ? false : true
            self.VC?.navigationController?.pushViewController(chatVC, animated: true)
//            self.VC?.performSegueWithIdentifier("MessageToChat", sender: true)
            
        }
        //        self.VC?.performSegueWithIdentifier("MessageToChat", sender: arrChats![indexPath.row].chatMessage)
    }
    
    func ignoreClicked(reqId: String, indexPath: NSIndexPath) {
        
        guard let userId = RKUserSingleton.sharedInstance.loggedInUser?.userId,token = RKUserSingleton.sharedInstance.loggedInUser?.userToken else { return }
        self.VC?.showLoader()
        Request.declineRequest(userId, reqId: reqId, token: token, success: { () -> () in
            
            
            self.tableView?.beginUpdates()
            self.arrRequests?.removeAtIndex(indexPath.row)
            self.tableView?.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Middle)
            self.tableView?.endUpdates()
            self.VC?.hideLoader()
            }, failure: { (error) -> () in
                print(error)
                self.VC?.hideLoader()
            }) { () -> () in
                self.VC?.hideLoader()
                self.VC?.logout()
        }

    }
 
    func acceptClicked(otherUserId: String,indexPath : NSIndexPath) {
        
        guard let userId = RKUserSingleton.sharedInstance.loggedInUser?.userId else { return }
        self.VC?.showLoader()
        Request.acceptRequest(userId, otherUserId: otherUserId, success: { (requests,chats) -> () in
            
            self.handleRequestAccept(requests,chats: chats,indexPath: indexPath)
            self.VC?.hideLoader()
            }, failure: { (error) -> () in
                self.VC?.hideLoader()
                self.VC?.showNativeAlert(error)
            }) { () -> () in
                self.VC?.hideLoader()
                RKUserSingleton.sharedInstance.loggedInUser = nil
                self.VC?.navigationController?.popToRootViewControllerAnimated(true)
        }

    }
    
    func handleRequestAccept(requests : [Request], chats : [Chat],indexPath : NSIndexPath){
        self.VC?.indexPath = indexPath
        self.arrRequests = requests
        self.arrChats = chats
        
        guard let chatVC = self.VC?.storyboard?.instantiateViewControllerWithIdentifier("ChatViewController") as? ChatViewController else { return }
        chatVC.userId2 = arrChats?.first?.chatSentTo
        chatVC.myImageUrl = arrChats?.first?.chatImage
        chatVC.otherUserName = arrChats?.first?.chatName
        chatVC.placeholderHidden = arrChats?.first?.chatMessage == "" ? false : true
        self.VC?.navigationController?.pushViewController(chatVC, animated: true)
//        self.VC?.performSegueWithIdentifier("MessageToChat", sender: false)
//        self.tableView?.reloadData()
    }
    func swipeTableCell(cell: MGSwipeTableCell!, tappedButtonAtIndex index: Int, direction: MGSwipeDirection, fromExpansion: Bool) -> Bool {
        guard let msgCell = cell as? ChatUserCell else { return true }
        guard let otherUserId = msgCell.currentChat?.chatSentTo,userId = msgCell.currentChat?.chatSentBy else { return false }
        guard let indexpath = self.arrChats?.indexOf(msgCell.currentChat!) else { return false }
        self.tableView?.beginUpdates()
        self.arrChats?.removeAtIndex(indexpath)
        self.tableView?.deleteRowsAtIndexPaths([NSIndexPath(forRow: indexpath + self.arrRequests!.count + 1, inSection: 0)], withRowAnimation: .Middle)
        self.tableView?.endUpdates()
        
        ChatDBManager.deleteMessages(otherUserId)
        
        guard let token = RKUserSingleton.sharedInstance.loggedInUser?.userToken else { return true }
       Message.deleteAllMessages(userId, otherUId: otherUserId, token: token, success: { () -> () in
        
        
        }, failure: { (error) -> () in
            
            
        }) { () -> () in
            
            RKUserSingleton.sharedInstance.loggedInUser = nil
            self.VC?.navigationController?.popToRootViewControllerAnimated(true)
        }
        return true
    }
    
    func viewProfile(userId: String?) {
        
        guard let id = userId,myId = RKUserSingleton.sharedInstance.loggedInUser?.userId,token = RKUserSingleton.sharedInstance.loggedInUser?.userToken else { return }
        self.VC?.showLoader()
        User.viewOtherUserProfile(myId, otherId: id, token: token, success: { (user) -> () in
            self.viewProfilehandler(user)
            self.VC?.hideLoader()
            }, failure: { (error) -> () in
                self.VC?.showNativeAlert(error)
                self.VC?.hideLoader()
            }) { () -> () in
                self.VC?.hideLoader()
                self.VC?.logout()
        }
    }
    
    func viewProfilehandler(user : User){
        
       self.profileView.currentUser = user
        showProfileView()
    }
    
    func viewOtherUserProfile(userId: String?) {
        self.viewProfile(userId)
    }
    
    func showProfileView(){
        self.VC?.view.addSubview(profileView)
        profileView.alpha = 0.0
        UIView.animateWithDuration(0.3) { () -> Void in
            
            self.profileView.alpha = 1.0
        }
    }
    
    func hideProfileView(){
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.profileView.alpha = 0.0
            }) { (completed) -> Void in
                self.profileView.removeFromSuperview()
        }
    }
    func dismissProfileView(){
        hideProfileView()
    }
    func showLinkedinProfile(userName : String,profileUrl : String){
        guard let VC = self.VC?.storyboard?.instantiateViewControllerWithIdentifier("LinkedinProfileViewController") as? LinkedinProfileViewController else { return }
        VC.linkedinProfileUrl = profileUrl
        VC.userName = userName
        self.VC?.presentViewController(VC, animated: true, completion: nil)
    }
}
