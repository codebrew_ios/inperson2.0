//
//  CameraGalleryPickerBlock.swift
//  Naseeb
//
//  Created by Night Reaper on 30/09/15.
//  Copyright (c) 2015 Gagan. All rights reserved.
//

import UIKit
import Photos

class CameraGalleryPickerBlock: NSObject , UIImagePickerControllerDelegate , UINavigationControllerDelegate{
   
    typealias onPicked = (UIImage,String) -> ()
    typealias onCanceled = () -> ()
    
    
    var pickedListner : onPicked?
    var canceledListner : onCanceled?
    
    
    
    class var sharedInstance: CameraGalleryPickerBlock {
        struct Static {
            static var instance: CameraGalleryPickerBlock?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = CameraGalleryPickerBlock()
        }
        
        return Static.instance!
    }
    
    override init(){
        super.init()
        
    }
    
    deinit
    {
        
    }

    
    func pickerImage( type type : String , presentInVc : UIViewController , pickedListner : onPicked , canceledListner : onCanceled){
       
        self.pickedListner = pickedListner
        self.canceledListner = canceledListner
        
        let picker : UIImagePickerController = UIImagePickerController()
        picker.sourceType = type == Constants.Camera ? UIImagePickerControllerSourceType.Camera : UIImagePickerControllerSourceType.PhotoLibrary
        picker.delegate = self
        picker.allowsEditing = false
        presentInVc.presentViewController(picker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        if let listener = canceledListner{
            listener()
        }
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        picker.dismissViewControllerAnimated(true, completion: nil)
        if let image : UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage,imageUrl = info[UIImagePickerControllerReferenceURL] , listener = pickedListner{
            listener(image.imageByScalingAndCroppingForSize(CGSizeMake(1200, 1200)),imageUrl.absoluteString)
        }else if let image : UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage, listener = pickedListner {
            
            listener(image.imageByScalingAndCroppingForSize(CGSizeMake(1200, 1200)),"")
        }
        
       
    }
    
   
    
    
}
