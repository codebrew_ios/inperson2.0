//
//  Message.swift
//  InPerson
//
//  Created by CB Macmini_3 on 10/01/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit
import SwiftyJSON
import PhotosUI


enum APIResponse   {
    
    case Success(AnyObject?)
    case Failure(String)
    
}

class Message: NSObject {

    var messageId : String?
    var messageSentBy : String?
    var messageSentTo : String?
    var message : String?
    var messageMedia : String?
    var messageIsRead : String?
    var messageDeleted : String?
    var messageToDeleted : String?
    var messageMediaName : String?
    var messageTime : String?
    var messageDate : String?
    var messageImageUrl : String?
    var messageCreatedAt : String?
    var messageImage : UIImage?
    
    init(attributes: Dictionary<String, JSON>) {
        super.init()
        
        messageId = attributes["id"]?.stringValue
        messageSentBy = attributes["sent_by"]?.stringValue
        messageSentTo = attributes["sent_to"]?.stringValue
        message = attributes["message"]?.stringValue
        messageMedia = attributes["media"]?.stringValue
        messageIsRead = attributes["is_read"]?.stringValue
        messageDeleted = attributes["deleted"]?.stringValue
        messageToDeleted = attributes["to_deleted"]?.stringValue
        messageMediaName = attributes["media_name"]?.stringValue
        messageTime = attributes["time"]?.stringValue
        messageDate = attributes["date"]?.stringValue
        messageImageUrl = attributes["sender_imageurl"]?.stringValue
        messageCreatedAt = attributes["created_at"]?.stringValue
        messageImage = nil
    }
    init(attributes: Dictionary<String, String>) {
        super.init()
        
        messageId = attributes["id"]
        messageSentBy = attributes["sent_by"]
        messageSentTo = attributes["sent_to"]
        message = attributes["message"]
        messageMedia = attributes["media"]
        messageIsRead = attributes["is_read"]
        messageDeleted = attributes["deleted"]
        messageToDeleted = attributes["to_deleted"]
        messageMediaName = attributes["media_name"]
        messageTime = attributes["time"]
        messageDate = attributes["date"]
        messageImageUrl = attributes["sender_imageurl"]
        messageCreatedAt = attributes["created_at"]
        if messageMedia!.containsString("assset"){
            
            let requestOptions = PHImageRequestOptions()
            requestOptions.resizeMode   = .Fast
            requestOptions.deliveryMode = .FastFormat
            requestOptions.synchronous = true
            
            PHImageManager.defaultManager().requestImageForAsset(PHAsset.fetchAssetsWithALAssetURLs([NSURL(string: messageMedia!)!], options: nil).firstObject as! PHAsset, targetSize: PHImageManagerMaximumSize, contentMode: .Default, options: requestOptions) { (image, info) -> Void in
                self.messageImage = image
            }

        }
        
    }

    
    class func convertJSONArrayToAirport(jsonArray : [JSON]) -> [Message]{
        
        var badgeArray : [Message] = []
        for dict in jsonArray {
            
            badgeArray.append(Message(attributes: dict.dictionaryValue))
        }
        
        return badgeArray
    }
    
    override init() {
        
        super.init()
    }

    
    class func pollOtherUserMessages(parameters : [String : String] ,url : String,success: ([Message]) -> (),failure:(String) -> (),tokenExpired: () -> ()){
        
        rClientApi.sharedInstance.postRequest(url, withParameters: parameters, success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        let messages = Message.convertJSONArrayToAirport(json["chat"].arrayValue)
                        success(messages)
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
            }

            }) { (error) -> () in
                
                failure(error.localizedDescription)
        }
    }
    
    class func sendMessage(parameters : [String : String] ,image : [UIImage]? ,url : String,success: (String,String) -> (),failure:(String) -> (),tokenExpired: () -> ()){
        let params = ["message":parameters["message"] ?? "","access_token":parameters["access_token"] ?? ""]
        
        rClientApi.sharedInstance.postFileWithParameters(url, paramerters: params , images: image, success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        success(json["msg_id"].stringValue,json["img"].stringValue)
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
            }
            }) { (error) -> () in
                
                 failure(error.localizedDescription)
        }
    }
    
    
    class func viewAllMessages(parameters : [String : String] ,url : String,success: ([Message]) -> (),failure:(String) -> (),tokenExpired: () -> ()){
        
        
        rClientApi.sharedInstance.postRequest(url, withParameters: parameters, success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        
                        let messages = Message.convertJSONArrayToAirport(json["chat"].arrayValue)
                        success(messages)
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
            }

            }) { (error) -> () in
                
                failure(error.localizedDescription)
        }
    }
    
    class func deleteAllMessages(userId : String ,otherUId : String,token : String, success: () -> (),failure:(String) -> (),tokenExpired:() -> ()){
        
        let url = URLS.BASE + userId + "/clearall/" + otherUId
        
        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token" : token], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        success()
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
            }

            }) { (error) -> () in
                
                failure(error.localizedDescription)
            }
    }
    
    class func convertDictionaryToModal (attributes attributes : Dictionary<String, String>? , image : UIImage?) -> Message{
        guard let attrib = attributes else { return Message() }
        let message : Message = Message()
        message.messageId = attrib["id"]
        message.messageSentBy = attrib["senderid"]
        message.messageSentTo = attrib["recieverid"]
        message.message = attrib["message"]
        message.messageImage = image
        message.messageTime = attrib["time"]
        message.messageDate = attrib["date"]
        message.messageImageUrl = attrib["sender_imageurl"]
        return message
    }
}
