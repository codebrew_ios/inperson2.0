//
//  User.swift
//  InPerson
//
//  Created by CB Macmini_3 on 17/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation
import SwiftyJSON

class User: NSObject {
    
    var userLinkedinId : String?
    var userName : String?
    var userEmail : String?
    var userImage : String?
    var userJob : String?
    var userCity : String?
    var userCompany : String?
    var userOtherData : String?
    var userToken : String?
    var userId : String?
    var userImageName : String?
    var userHere : String?
    var userConnected : String?
    var userProfileUrl : String?
    
    var founder : String?
    
    var userHide : String?
    var userPush : String?
    var userLoc : String?
   
   
    init(attributes: Dictionary<String, JSON>) {
        super.init()
        
        userId = attributes["id"]?.stringValue
        userLinkedinId = attributes["linkedin_id"]?.stringValue
        userToken = attributes["access_token"]?.stringValue
        userName = attributes["name"]?.stringValue
        userEmail = attributes["email"]?.stringValue
        userImage = attributes["image"]?.stringValue
        userJob = attributes["job_title"]?.stringValue
        userCity = attributes["city"]?.stringValue
        userCompany = attributes["company"]?.stringValue
        userOtherData = attributes["otherdata"]?.stringValue
        userImageName = attributes["image"]?.stringValue
        userHide = attributes["hidden"]?.stringValue
        userPush = attributes["push"]?.stringValue
        userLoc = attributes["loc"]?.stringValue
        userProfileUrl = attributes["profile_url"]?.stringValue
        
        founder = attributes["founder"]?.stringValue
        
        guard let here = attributes["here"]?.stringValue , connected = attributes["connected"]?.stringValue else { return }
        
        userHere = here
        userConnected = connected
    }
    
    override init() {
        
        super.init()
    }
    
    class func convertJSONArrayToUserArray(jsonArray : [JSON]) -> [User]{
        
        var badgeArray : [User] = []
        for dict in jsonArray {
            
            badgeArray.append(User(attributes: dict.dictionaryValue))
        }
        
        return badgeArray
    }
    
    class func getUserProfile(userId : String, token : String, success: (User,Bool?,Bool?,Bool?) -> (), failure: (String) -> (),tokenExpired: () -> ()){
        
        let completePath = URLS.BASE + userId + "/profile"
        rClientApi.sharedInstance.postRequest(completePath, withParameters: ["access_token" : token, "userid" : userId], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                     tokenExpired()
                    }
                    let userInfo = json["user"].dictionaryValue
                    let user = User(attributes: userInfo)
                    RKUserSingleton.sharedInstance.loggedInUser = user
                    success(user,userInfo["hidden"]?.boolValue,userInfo["push"]?.boolValue,userInfo["loc"]?.boolValue)
                }
                else{
                    failure(json["msg"].stringValue)
                }
            }
            }) { (error) -> () in
                
                
        }
    }
    class func getUsers(userId : String, airId : String, token : String,lat : String,lng : String
        , success: ([User],[ContentCard],String,String) -> () , failure: (String) -> (),tokenExpired: () -> ()){
//        user/{userid}/viewusers/{airid}
        
        let url = URLS.BASE + userId + "/viewusers/" + airId
        
            rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token":token, "userid":userId, "airid" : airId, "lat" : lat, "lng" : lng], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess {
                    let tokenExp = json["success"].stringValue
                     if tokenExp == "-1" { tokenExpired() }
                    let user = User(attributes: json["my_profile"].dictionaryValue)
                    RKUserSingleton.sharedInstance.loggedInUser = user
                    let cards = ContentCard.convertJSONArrayToCardArray(json["cards"].arrayValue)
                    let arrusers = User.convertJSONArrayToUserArray(json["users"].arrayValue)
                    success(arrusers,cards,json["unread_count"].stringValue,json["check"].stringValue)
                }
                else{
                    
                    failure(json["msg"].stringValue)
                }
                
            }

                }) { (error) -> () in
                    
                    failure(error.localizedDescription)
            }
    }
    
    class func getUsersFirstTime(userId : String, airId : String, token : String,lat : String,lng : String
        , success: ([User],[ContentCard],String,String) -> () , failure: (String) -> (),tokenExpired: () -> ()){
            
            
            let url = URLS.BASE + userId + "/all"
            
            rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token":token, "userid":userId, "lat" : lat, "lng" : lng], success: { (response) -> () in
                
                if let data: AnyObject = response{
                    var json = JSON(data)
                    print(json)
                    let ressuccess = json["success"].boolValue
                    if ressuccess {
                        let tokenExp = json["success"].stringValue
                        if tokenExp == "-1" {
                            tokenExpired()
                            return
                        }
                        let airport = Airport(attributes: json["airport"].dictionaryValue)
                        let user = User(attributes: json["my_profile"].dictionaryValue)
                        let cards = ContentCard.convertJSONArrayToCardArray(json["cards"].arrayValue)
                        RKUserSingleton.sharedInstance.loggedInUser = user
                        RKUserSingleton.sharedInstance.selectedAirport = airport
//                        NSUserDefaults.standardUserDefaults().rm_setCustomObject(airport, forKey: "savedAirport")
                        let arrusers = User.convertJSONArrayToUserArray(json["users"].arrayValue)
                        success(arrusers,cards,json["unread_count"].stringValue,json["check"].stringValue)
                    }
                    else{
                        
                        failure(json["msg"].stringValue)
                    }
                    
                }
                
                }) { (error) -> () in
                    
                    failure(error.localizedDescription)
            }
            
    }
    
    class func getNearestAirports(userId : String , token : String, lat : String , lng : String, change : String ,success: ([Airport]) -> (), failure : (String,Bool) -> ()){
//        user/{userid}/home
        
        let url = URLS.BASE + userId + "/home"
        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token":token, "userid":userId, "lat":lat, "lng":lng, "change":change], success: { (response) -> () in
        
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess && json["success"].stringValue != "-1"{
                    
                    let airportArray = Airport.convertJSONArrayToAirport(json["airports"].arrayValue)
                    success(airportArray)
                }
                else{
                    failure(json["msg"].stringValue,json["success"].boolValue)
                }
            }
            }) { (error) -> () in
                
                failure(error.localizedDescription,false)
        }
    }
    class func changeUserSettings(userId : String,token : String,hide : String,push : String,loc : String,success:(User) -> (),failure: (String) -> (),tokenExpired:() -> ()){
        
        let url = URLS.BASE + userId + "/setting"

        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token":token, "userid":userId, "hide":hide, "push":push, "loc":loc], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        let userInfo = json["user"].dictionaryValue
                        let user = User(attributes: userInfo)
                        RKUserSingleton.sharedInstance.loggedInUser = user
                        success(user)
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
                
            }
            }) { (error) -> () in
                
                failure(error.localizedDescription)
        }

    }
    
    class func connectUser(userId : String, otherUserId : String, success: () -> (), failure: (String) -> () ,tokenExpired: () -> ()){
        
//        user/{userid}/connect/{otherid}
        guard let token = RKUserSingleton.sharedInstance.loggedInUser?.userToken, currentAirport = RKUserSingleton.sharedInstance.selectedAirport,location = RKLocationManager.sharedInstance.currentLocation else { return }
        let url = URLS.BASE + userId + "/connect/" + otherUserId
        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token" : token,"airid" : currentAirport.airportId!,"lat" : location.current_lat!,"lng" : location.current_lng!], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        
                        success()
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
                
            }
            }) { (error) -> () in
                
                failure(error.localizedDescription)
        }
    }
    
    class func getAllMessagelisting(userId : String,token : String,zone : String,success: ([Request],[Chat],Chat) -> (), failure: (String) -> (),tokenExpired : () -> ()){

        let url = URLS.BASE + userId + "/viewcc"
        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token":token,"zone" : zone ], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        let adminMessage = Chat.convertJSONArrayToRequestArray(json["admin_message"].arrayValue)
                        let requests = Request.convertJSONArrayToRequestArray(json["requests"].arrayValue)
                        let chats = Chat.convertJSONArrayToRequestArray(json["chat"].arrayValue)
                        success(requests,chats,adminMessage.first!)
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
                
            }

            }) { (error) -> () in
                
               failure(error.localizedDescription)
        }
    }
    
    class func addUserToAirport(userId : String, airId : String,lat : String,lng : String, token : String, success: (Bool) -> (),failure: (String) -> (),tokenExpired: () -> ()){
        
        let url = URLS.BASE + userId + "/here/" + airId
        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token" : token,"lat" : lat, "lng" : lng], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        
                        success(json["stop"].boolValue)
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
            }
            
            }) { (error) -> () in
                
                failure(error.localizedDescription)
            }
    }
    
    class func blockOtherUser(otherUserId : String,success: () -> (),failure: (String) -> (),tokenExpired: ()-> ()){
        
        guard let me = RKUserSingleton.sharedInstance.loggedInUser,myId = me.userId,token = me.userToken else { return }
        let url = URLS.BASE + myId + "/block/" + otherUserId
        
        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token" : token], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        
                        success()
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
            }

            }) { (error) -> () in
                failure(error.localizedDescription)
        }
    }
    
    class func viewOtherUserProfile(myId : String,otherId : String,token : String,success: (User) -> (),failure: (String) -> (),tokenExpired : () -> ()){
        
        let url = URLS.BASE + myId + "/view/" + otherId
        
        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token" : token], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        
                        let user = User(attributes: json["user"].dictionaryValue)
                        success(user)
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
            }

            }) { (error) -> () in
                
                failure(error.localizedDescription)
        }
    }
    
    class func registerDeviceToken(){
//        deviceToken = RKUserSingleton.sharedInstance.deviceToken
        guard let token = RKUserSingleton.sharedInstance.loggedInUser?.userToken,myID = RKUserSingleton.sharedInstance.loggedInUser?.userId else { return }
        let url = URLS.BASE + myID + "/savegcm"
        
        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token" : token,"device_id" : RKUserSingleton.sharedInstance.deviceToken ?? "abc"], success: { (response) -> () in
            
            }) { (error) -> () in
            
        }
    }
}


class Airport : NSObject {
    
    var airportId : String?
    var airportName : String?
    var airportCity : String?
    var airportCountry : String?
    var airportCode : String?
    
    init(attributes: Dictionary<String, JSON>) {
        super.init()
        
        airportId = attributes["id"]?.stringValue
        airportName = attributes["name"]?.stringValue
        airportCity = attributes["city"]?.stringValue
        airportCode = attributes["code"]?.stringValue
        airportCountry = attributes["country"]?.stringValue
    }
    
    override init() {
        
        super.init()
    }

    
    class func convertJSONArrayToAirport(jsonArray : [JSON]) -> [Airport]{
        
        var badgeArray : [Airport] = []
        for dict in jsonArray {
            
            badgeArray.append(Airport(attributes: dict.dictionaryValue))
        }
        
        return badgeArray
    }
    
    class func searchAirport(userId : String, token : String, searchKey : String ,success:([Airport]) -> (), failure: (String) -> (),tokenExpired: () -> ()){
        
//        user/{userid}/search
        
        let url = URLS.BASE + userId + "/search"
        
        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token": token, "userid" : userId, "search" : searchKey], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    
                    if json["success"].stringValue == "-1" { tokenExpired() }
                    let airportArray = Airport.convertJSONArrayToAirport(json["airports"].arrayValue)
                    success(airportArray.count > 0 ? airportArray : [])
                }
                else{
                    failure(json["msg"].stringValue)
                }
            }

            }) { (error) -> () in
                
                failure(error.localizedDescription)
        }
    }
}

class Request : NSObject {
    
    var user1Id : String?
    var user2Id : String?
    var requestMessage : String?
    var requestId : String?
    var requestUserName : String?
    var requestUserImage : String?
    var requestTime : String?
    
    
    init(attributes: Dictionary<String, JSON>) {
        super.init()
        
        user1Id = attributes["id"]?.stringValue
        user2Id = attributes["user_id"]?.stringValue
        requestMessage = attributes["message"]?.stringValue
        requestId = attributes["request_id"]?.stringValue
        requestUserName = attributes["user_name"]?.stringValue
        requestUserImage = attributes["user_image"]?.stringValue
        requestTime = attributes["timeago"]?.stringValue
    }
    
    override init() {
        
        super.init()
    }
    
    class func convertJSONArrayToRequestArray(jsonArray : [JSON]) -> [Request]{
        
        var badgeArray : [Request] = []
        for dict in jsonArray {
            
            badgeArray.append(Request(attributes: dict.dictionaryValue))
        }
        
        return badgeArray
    }

    class func acceptRequest(userId : String ,otherUserId : String ,success: ([Request],[Chat]) -> (), failure: (String) -> (), tokenExpired: () -> () ){
        
        guard let token = RKUserSingleton.sharedInstance.loggedInUser?.userToken, currentAirport = RKUserSingleton.sharedInstance.selectedAirport,location = RKLocationManager.sharedInstance.currentLocation else { return }
        let url = URLS.BASE + userId + "/connect-with/" + otherUserId
        
        let timezone = NSTimeZone.localTimeZone()
        let zone = timezone.name
        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token" : token,"airid" : currentAirport.airportId!,"lat" : location.current_lat!,"lng" : location.current_lng!,"zone" : zone], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        let requests = Request.convertJSONArrayToRequestArray(json["requests"].arrayValue)
                        let chats = Chat.convertJSONArrayToRequestArray(json["chat"].arrayValue)
                        
                        success(requests,chats)
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
                
            }
            }) { (error) -> () in
                
                failure(error.localizedDescription)
        }

    }
    
    
    class func declineRequest(userId : String,reqId : String, token : String,success: () -> (), failure: (String) -> (), tokenExpired: () -> ()){
        
        let url = URLS.BASE + userId + "/request/" + reqId + "/decline"
       
        rClientApi.sharedInstance.postRequest(url, withParameters: ["access_token" : token], success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    if json["success"].stringValue == "-1"{
                        tokenExpired()
                    }else{
                        
                        success()
                    }
                }
                else{
                    failure(json["msg"].stringValue)
                }
                
            }

            }) { (error) -> () in
                
            failure(error.localizedDescription)
        }
    }
}


class Chat : NSObject {

    var chatSentTo : String?
    var chatSentBy : String?
    var chatMessage : String?
    var chatMedia : String?
    var chatTime : String?
    var chatDeleted : String?
    var chatId : String?
    var chatName : String?
    var chatImage : String?
    var chatUnreadCount : String?
    
    
    init(attributes: Dictionary<String, JSON>) {
        super.init()
        
        chatSentTo = attributes["sent_to"]?.stringValue
        chatSentBy = attributes["sent_by"]?.stringValue
        chatMessage = attributes["message"]?.stringValue
        chatMedia = attributes["media"]?.stringValue
        chatTime = attributes["created_at"]?.stringValue
        chatDeleted = attributes["deleted"]?.stringValue
        chatId = attributes["id"]?.stringValue
        chatName = attributes["name"]?.stringValue
        chatImage = attributes["image"]?.stringValue
        chatUnreadCount = attributes["unread_count"]?.stringValue
    }
    
    override init() {
        
        super.init()
    }
    
    class func convertJSONArrayToRequestArray(jsonArray : [JSON]) -> [Chat]{
        
        var badgeArray : [Chat] = []
        for dict in jsonArray {
            
            badgeArray.append(Chat(attributes: dict.dictionaryValue))
        }
        
        return badgeArray
    }
    
    
}

