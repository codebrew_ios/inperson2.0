//
//  Push.swift
//  InPerson
//
//  Created by Apple on 10/03/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

class Push: NSObject {
    
    var type : String?
    
    var senderId : String?
    var recieverId : String?
    var unreadMessages : String?
    var senderName : String?
    var senderProfilePic : String?
    var allUnreadMessages : String?
    
    var userId : String?
    var userName : String?
    var userImageUrl : String?
    var timeAgo : String?
    var badgeCount : String?
    


    init(pushInfo : NSDictionary) {
        let pi = pushInfo 
        let t = pi.valueForKeyPath("aps.alert.loc-args.t")
        type = "\(t!)"
        
        if type == "1" {
            
            senderId = pi.valueForKeyPath("aps.alert.loc-args.s_id") as? String
            recieverId = pi.valueForKeyPath("aps.alert.loc-args.r_id") as? String
            unreadMessages = "\(pi.valueForKeyPath("aps.alert.loc-args.c")!)"
            senderName = pi.valueForKeyPath("aps.alert.loc-args.n") as? String
            senderProfilePic = pi.valueForKeyPath("aps.alert.loc-args.p") as? String
            allUnreadMessages = "\(pi.valueForKeyPath("aps.alert.loc-args.td")!)"
            badgeCount = "\(pi.valueForKeyPath("aps.alert.loc-args.td")!)"
        }else{
            
            userId = pi.valueForKeyPath("aps.alert.loc-args.u_id") as? String
            userName = pi.valueForKeyPath("aps.alert.loc-args.u_n") as? String
            userImageUrl = pi.valueForKeyPath("aps.alert.loc-args.u_p") as? String
            timeAgo = pi.valueForKeyPath("aps.alert.loc-args.tm") as? String
            badgeCount = "\(pi.valueForKeyPath("aps.alert.loc-args.td")!)"
        }
    }
    
    override init() {
        super.init()
    }
}
