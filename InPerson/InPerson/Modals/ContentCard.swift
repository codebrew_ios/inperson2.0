//
//  ContentCard.swift
//  InPerson
//
//  Created by Apple on 14/03/16.
//  Copyright © 2016 Code Brew Labs. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContentCard: NSObject {
    
    var id : String?
    var category : String?
    var title : String?
    var image : String?
    var link : String?
    
    init(attributes: Dictionary<String, JSON>) {
        super.init()
        id = attributes["id"]?.stringValue
        category = attributes["category"]?.stringValue
        title = attributes["title"]?.stringValue
        image = attributes["image"]?.stringValue
        link = attributes["link"]?.stringValue
    }
    
    override init() {
        
        super.init()
    }

    class func convertJSONArrayToCardArray(jsonArray : [JSON]) -> [ContentCard]{
        
        var badgeArray : [ContentCard] = []
        for dict in jsonArray {
            
            badgeArray.append(ContentCard(attributes: dict.dictionaryValue))
        }
        
        return badgeArray
    }
    
}
