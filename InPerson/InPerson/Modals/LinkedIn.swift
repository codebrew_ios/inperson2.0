//
//  LinkedIn.swift
//  InPerson
//
//  Created by CB Macmini_3 on 17/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation
import SwiftyJSON
//import IOSLinkedInAPI
import RMMapper

class LinkedIn: NSObject {

    
    var linkedin_email : String?
    var linkedin_id : String?
    var linkedin_pic : String?
    var linkedin_summary : String?
    var linkedin_name : String?
    var linkedin_position : String?
    var linkedin_company : String?
    var linkedin_location : String?
    var linkedin_profileUrl : String?
    var linkedin_headline : String?
    
    init(attributes: Dictionary<String, JSON>) {
        super.init()
        self.linkedin_email = attributes["emailAddress"]?.stringValue
        self.linkedin_id = attributes["id"]?.stringValue
        
        self.linkedin_pic = attributes["pictureUrl"]?.stringValue
        self.linkedin_summary = attributes["summary"]?.stringValue
        self.linkedin_name = attributes["formattedName"]?.stringValue
        self.linkedin_profileUrl = attributes["publicProfileUrl"]?.stringValue
        self.linkedin_headline = attributes["headline"]?.stringValue
        guard let picsDict = attributes["pictureUrls"]?.dictionaryValue,arrPics = picsDict["values"]?.arrayValue else { return }
       self.linkedin_pic = arrPics.last?.stringValue
        if let positions = attributes["positions"]?.dictionaryValue, currentPosition = positions["values"]?.arrayValue.last?.dictionaryValue, company = currentPosition["company"]?.dictionaryValue{
             linkedin_position = currentPosition["title"]?.stringValue
            linkedin_company = company["name"]?.stringValue
        
        }
        if let  location = attributes["location"]?.dictionaryValue , place = location["name"]?.stringValue{
             linkedin_location = place
        }
    }
    
    override init() {
        
        super.init()
    }
    
    class func userLinkedInLogin (params : [String : String] ,image : String , success : (User) -> () , failure : (String) -> ()){
        
        let profilePic : UIImage
        if let imageurl : NSURL = NSURL(string: image), data = NSData(contentsOfURL: imageurl) {
            profilePic = UIImage(data: data)!
        }else{
            profilePic = UIImage()
        }
        
        
        rClientApi.sharedInstance.postFileWithParameters(URLS.BASE + URLS.login, paramerters: params, profilePic: profilePic, success: { (response) -> () in
            
            if let data: AnyObject = response{
                var json = JSON(data)
                let ressuccess = json["success"].boolValue
                if ressuccess{
                    let user = User(attributes: json["user"].dictionaryValue)
                    RKUserSingleton.sharedInstance.loggedInUser = user
                    NSUserDefaults.standardUserDefaults()
                    .setBool(json["new_register"].boolValue, forKey: "IPMessageBanner")
                    success(user)
                }
                else{
                    failure(json["msg"].stringValue)
                }
                
                
            }
            
            }) { (error) -> () in
                
                failure(error.localizedDescription)
        }
               
    }
    
    
    class func linkedInInfo ( accessToken : String  , success  : (LinkedIn) -> () , failure : () -> () ){
 
        let url : String = NSString(format: "https://api.linkedin.com/v1/people/~:(id,formatted-name,num-connections,picture-url,email-address,specialties,summary,location:(name),industry,network,site-standard-profile-request,public-profile-url,picture-urls::(original),skills,positions,headline)?oauth2_access_token=%@&format=json",  accessToken) as String

//        let url : String = NSString(format: "https://api.linkedin.com/v1/people/~?oauth2_access_token=%@&format=json",  accessToken) as String
        rClientApi.sharedInstance.postPath(url, withParameters: ["" : ""], success: { (data) -> () in
            
            if let data: AnyObject = data{
                let json = JSON(data)
                
                let user = LinkedIn(attributes: json.dictionaryValue)
                success(user)
            }
            else{
                failure()
            }
            
            }) { (error) -> () in
                failure()
                
        }
        
        
        
    }

}
