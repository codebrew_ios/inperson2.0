//
//  ChatDBManager.swift
//  InPerson
//
//  Created by CB Macmini_3 on 11/01/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit
import PhotosUI
import CoreData

class ChatDBManager: NSObject {
    
    
    let myId = RKUserSingleton.sharedInstance.loggedInUser?.userId ?? ""
    
    //MARK: - Persistency Manager
     func saveMessage(currentMessage : Message) {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        
        
        let entity =  NSEntityDescription.entityForName("Chat",
            inManagedObjectContext:managedContext)
        
        let message = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext: managedContext)
        
        
        message.setValue(currentMessage.messageId, forKey: "id")
        message.setValue(currentMessage.message, forKey: "message")
        message.setValue(currentMessage.messageMedia, forKey: "media")
        message.setValue(currentMessage.messageDate, forKey: "date")
        message.setValue(currentMessage.messageTime, forKey: "time")
        message.setValue(currentMessage.messageImageUrl, forKey: "sender_imageurl")
        message.setValue(currentMessage.messageSentBy, forKey: "sent_by")
        message.setValue(currentMessage.messageSentTo, forKey: "sent_to")
        
        //TODO :  
//        message.setValue(currentMessage.messageCreatedAt, forKey: "deleted")
        message.setValue(currentMessage.messageToDeleted, forKey: "to_deleted")
        message.setValue(currentMessage.messageCreatedAt, forKey: "created_at")

        do {
            
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
     func retrieveSavedLoggedInMessages(oUserId : String) -> [Message] {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Chat")
        fetchRequest.returnsDistinctResults = true
        
        do {
            let results =
            try managedContext.executeFetchRequest(fetchRequest) as! [NSManagedObject]
       
           let msgs = self.getMessageForUser(oUserId, arrManagedObject: results )
            return msgs
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return [Message()]
    }
    
    class func deleteAllFromEntityName(entityName entityName : String){
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Chat")
        if #available(iOS 9.0, *) {
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            do {
                try managedContext.executeRequest(deleteRequest)
            } catch _ as NSError {
                // TODO: handle the error
            }
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    private func convertManagedContextToMessage(mc : NSManagedObject) -> Message{
        
        let msg = Message()
        msg.messageId = mc.valueForKey("id") as? String
        msg.message = mc.valueForKey("message") as? String
        msg.messageMedia = mc.valueForKey("media") as? String
        msg.messageDate = mc.valueForKey("date") as? String
        msg.messageTime = mc.valueForKey("time") as? String
        msg.messageImageUrl = mc.valueForKey("sender_imageurl") as? String
        msg.messageSentBy = mc.valueForKey("sent_by") as? String
        msg.messageSentTo = mc.valueForKey("sent_to") as? String
//        msg.messageDeleted = mc.valueForKey("deleted") as? String
        msg.messageToDeleted = mc.valueForKey("to_deleted") as? String
        msg.messageCreatedAt = mc.valueForKey("created_at") as? String
        
        return msg
    }
    
    
    private func getMessageForUser(id : String,arrManagedObject : [NSManagedObject]) -> [Message]{
        
        var msgs : [Message] = []
        for mc in arrManagedObject {
            
            if let sentBy = mc.valueForKey("sent_by") as? String ,sentTo = mc.valueForKey("sent_to") as? String{
                
                if (sentBy == myId && sentTo == id) || (sentTo == myId && sentBy == id) {
                    
                    msgs.append(convertManagedContextToMessage(mc))
                }
            }
        }
        
        return msgs
    }
    
    
    class func deleteMessages(userId : String){
        
        let myId = RKUserSingleton.sharedInstance.loggedInUser?.userId ?? ""
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Chat")
        
        do {
            let results =
            try managedContext.executeFetchRequest(fetchRequest) as! [NSManagedObject]
            
            for result in results {
                if let sentBy = result.valueForKey("sent_by") as? String ,sentTo = result.valueForKey("sent_to") as? String{
                    
                    if (sentBy == myId && sentTo == userId) || (sentTo == myId && sentBy == userId) {
                        managedContext.deleteObject(result)
                    }
                }
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func saveImageToDocumentsDirectory(image : UIImage,userID : String) -> String{
        
        let imagePath = getDocumentsURL().URLByAppendingPathComponent(String(format: "image%@%lld", arguments: [userID,random() % (1000-100000000)]))
        saveImage(image, path: imagePath.path!)
            return imagePath.path!
    }
    func fileInDocumentsDirectory(filename: String) -> String {
        
        let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
        return fileURL.path!
        
    }
    
    func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    func saveImage (image: UIImage, path: String ) -> Bool{
        
        //            let pngImageData = UIImagePNGRepresentation(image)
        let jpgImageData = UIImageJPEGRepresentation(image, 1.0)   // if you want to save as JPEG
        let result = jpgImageData!.writeToFile(path, atomically: true)
        
        return result
    }
}
