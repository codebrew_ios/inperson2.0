//
//  Constants.swift
//  InPerson
//
//  Created by CB Macmini_3 on 17/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation

let appColor = UIColor(red: 10.0/255.0, green: 126.0/255.0, blue: 252.0/255.0, alpha: 1.0)
struct LinkedInC {
    
    static let LinkedIn_ClientId = "75aoy5w5l67zep"
    static let LinkedIn_ClientSecret = "4BVIpk5Ujt7iIiXN"
    static let LinkedIn_state = "DCEEFWF45453sdffef424"
    static let LinkedIn_Redirect_url = "https://www.code-brew.com"
    static let LinkedIn_Permissions =  ["r_basicprofile", "r_emailaddress"]
}

struct URLS {
    
//    static let BASE = "http://code-brew.com/projects/Inperson/public/user/"
    static let BASE = "http://52.37.13.12/user/"
    static let ImageBASE = "http://code-brew.com/projects/Inperson/public/resize/"
    static let login = "login"
}

struct CellIdentifiers {
    
    static let MyTextCell = "SenderCell"
    static let MyImageCell = "SenderImageCell"
    static let OTextCell = "RecieverCell"
    static let OImageCell = "RecieverImageCell"
}
struct Constants{
    static let Camera = "Camera"
    static let PhotoLibrary = "Photo Library"
}
struct ScreenSize{
    
    static let SCREEN_WIDTH = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType{
    
    static let IS_IPHONE_4_OR_LESS =  UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
}
