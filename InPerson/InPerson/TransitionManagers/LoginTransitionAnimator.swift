//
//  LoginTransitionAnimator.swift
//  ZDT_ExpandCircle
//
//  Created by Sztanyi Szabolcs on 27/02/15.
//  Copyright (c) 2015 Sztanyi Szabolcs. All rights reserved.
//

import UIKit

class LoginTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
   
    var transitionContext: UIViewControllerContextTransitioning?
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.3
    }
 
    /**
        With masking transition
    */
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        self.transitionContext = transitionContext
        
        guard let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? HomeViewController else { return }
        let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        
        let contView = transitionContext.containerView()
        
        let button = fromVC.btnAnimator
        
        
        let maskStartBP = UIBezierPath(ovalInRect: button.frame)
        
        contView?.addSubview(fromVC.view)
        
        contView?.addSubview(toVC!.view)
        
        
        let finalPoint : CGPoint
        if(button.frame.origin.x > (toVC!.view.bounds.size.width / 2)){
            if (button.frame.origin.y < (toVC!.view.bounds.size.height / 2)) {
                finalPoint = CGPointMake(button.center.x - 0, button.center.y - CGRectGetMaxY(toVC!.view.bounds)+30);
            }else{
                finalPoint = CGPointMake(button.center.x - 0, button.center.y - 0);
            }
        }else{
            if (button.frame.origin.y < (toVC!.view.bounds.size.height / 2)) {
                finalPoint = CGPointMake(button.center.x - CGRectGetMaxX(toVC!.view.bounds), button.center.y - CGRectGetMaxY(toVC!.view.bounds)+30);
            }else{
                finalPoint = CGPointMake(button.center.x - CGRectGetMaxX(toVC!.view.bounds), button.center.y - 0);
            }
        }
        
        
        
        let radius = sqrt((finalPoint.x * finalPoint.x) + (finalPoint.y * finalPoint.y))
        let maskFinalBP = UIBezierPath(ovalInRect:CGRectInset(button.frame, -radius, -radius))
        
 
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskFinalBP.CGPath
        toVC!.view.layer.mask = maskLayer
        
        
        let maskLayerAnimation = CABasicAnimation(keyPath: "path")
        maskLayerAnimation.fromValue = maskStartBP.CGPath
        maskLayerAnimation.toValue = maskFinalBP.CGPath
        maskLayerAnimation.duration = self.transitionDuration(transitionContext)
        maskLayerAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        maskLayerAnimation.delegate = self
        
        maskLayer.addAnimation(maskLayerAnimation, forKey: "path")
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if let transitionContext = self.transitionContext {
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
            
            let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as! HomeViewController
            let destinationController: UIViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
            fromViewController.view.layer.mask = nil
            destinationController.view.layer.mask = nil
        }
    }
}
