//
//  LoginTransitionAnimatorDismiss.swift
//  ZDT_ExpandCircle
//
//  Created by Sztanyi Szabolcs on 13/03/15.
//  Copyright (c) 2015 Sztanyi Szabolcs. All rights reserved.
//

import UIKit

class LoginTransitionAnimatorDismiss: NSObject, UIViewControllerAnimatedTransitioning {

    var transitionContext: UIViewControllerContextTransitioning?
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.4
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        self.transitionContext = transitionContext
        
        let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        guard let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)  as? HomeViewController else { return }
        let containerView = transitionContext.containerView()
        let button = toVC.btnAnimator
        
        containerView?.addSubview(toVC.view)
        containerView?.addSubview(fromVC!.view)
        
        let finalPath = UIBezierPath(ovalInRect: button.frame)
        
        var finalPoint : CGPoint
        
 
        if(button.frame.origin.x > (toVC.view.bounds.size.width / 2)){
            if (button.frame.origin.y < (toVC.view.bounds.size.height / 2)) {

                finalPoint = CGPointMake(button.center.x - 0, button.center.y - CGRectGetMaxY(toVC.view.bounds)+30);
            }else{

                finalPoint = CGPointMake(button.center.x - 0, button.center.y - 0);
            }
        }else{
            if (button.frame.origin.y < (toVC.view.bounds.size.height / 2)) {
                finalPoint = CGPointMake(button.center.x - CGRectGetMaxX(toVC.view.bounds), button.center.y - CGRectGetMaxY(toVC.view.bounds)+30);
            }else{
                finalPoint = CGPointMake(button.center.x - CGRectGetMaxX(toVC.view.bounds), button.center.y - 0);
            }
        }
        
        
        
        let radius = sqrt(finalPoint.x * finalPoint.x + finalPoint.y * finalPoint.y)
        let startPath = UIBezierPath(ovalInRect: CGRectInset(button.frame, -radius, -radius))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = finalPath.CGPath
        fromVC!.view.layer.mask = maskLayer
        
        
        let pingAnimation = CABasicAnimation(keyPath: "path")
        pingAnimation.fromValue = startPath.CGPath
        pingAnimation.toValue   = finalPath.CGPath
        pingAnimation.duration = self.transitionDuration(transitionContext)
        pingAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        pingAnimation.delegate = self;
        maskLayer.addAnimation(pingAnimation, forKey: "pingInvert")
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if let transitionContext = self.transitionContext {
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
            
            let destinationController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as! HomeViewController
            let fromViewController : UIViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
            fromViewController.view.layer.mask = nil
            destinationController.view.layer.mask = nil
        }
    }
}
