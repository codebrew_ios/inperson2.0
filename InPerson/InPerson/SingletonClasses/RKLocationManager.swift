//
//  RKLocationManager.swift
//  InPerson
//
//  Created by CB Macmini_3 on 17/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation
import CoreLocation
//import AFNetworking

struct Location {
    var current_lat : String?
    var current_lng : String?
    var current_formattedAddr : String?
    var current_city : String?
}

struct DeviceTime {
    
    var currentTime : String {
        get{
            return  localTimeFormatter.stringFromDate(NSDate())
        }
    }
    
    var currentDate : String{
        get{
            return localDateFormatter.stringFromDate(NSDate())
        }
    }
    
    var current_zone : String {
        get {
            return NSTimeZone.localTimeZone().name
        }
    }
    
    
    
    func convertCreated_at (created_at : String?) -> NSDate?{
        return localDateTimeFormatter.dateFromString(created_at ?? "")
    }
    
    
    var localTimeZoneFormatter = NSDateFormatter()
    var localTimeFormatter = NSDateFormatter()
    var localDateFormatter = NSDateFormatter()
    
    var localDateTimeFormatter = NSDateFormatter()
    
    func setUpTimeFormatters(){
        localTimeFormatter.dateFormat = "h:mm a"
        localDateFormatter.dateFormat = "MMM dd yyyy"
        localDateTimeFormatter.dateFormat = "MMM dd yyyy h:mm a Z"
        
    }
}

protocol RKLocationManagerDelegate{
    
    func locationUpdated(location : Location?)
}

class RKLocationManager: NSObject, CLLocationManagerDelegate {

    var delegate : RKLocationManagerDelegate?
    let locationManager = CLLocationManager()
    var currentLocation : Location? = Location()
    var currentTime : DeviceTime? = DeviceTime()
    class var sharedInstance: RKLocationManager {
        struct Static {
            static var instance: RKLocationManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = RKLocationManager()
        }
        
        return Static.instance!
    }

    
    var ISINTERNET_AVAILABLE = true {
        didSet{
            self.checkFrInternet()
        }
    }
    
    func startTrackingUser(){
        
        currentTime?.setUpTimeFormatters()
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()           
        }
    }
    
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print(error.localizedDescription)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        let locValue:CLLocationCoordinate2D = (manager.location?.coordinate) ?? CLLocationCoordinate2D()
        if let _ = self.currentLocation?.current_lat {
            self.currentLocation?.current_lat = "\(locValue.latitude)"
            self.currentLocation?.current_lng = "\(locValue.longitude)"
        }else{
            self.currentLocation?.current_lat = "\(locValue.latitude)"
            self.currentLocation?.current_lng = "\(locValue.longitude)"
            self.delegate?.locationUpdated(self.currentLocation)
        }
        
        guard let location = locations.last else{
            return
        }
        
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) -> Void in
            if let mark = placemarks?.last , addrList = mark.addressDictionary {
                self.formatAddress(addrList)
            }
        }
    }
    
    
    private func checkFrInternet(){
        AFNetworkReachabilityManager.sharedManager().startMonitoring()
        AFNetworkReachabilityManager.sharedManager().setReachabilityStatusChangeBlock { (status) -> Void in
            
            if status == AFNetworkReachabilityStatus.NotReachable {
                // Not reachable
                
                self.ISINTERNET_AVAILABLE = false
//                SCLAlertView().showWarning(KInternetConnectionTitle, subTitle: KInternetConnectionMessage, closeButtonTitle: "Close", duration: 5.0)
                
            } else {
                // Reachable
                self.ISINTERNET_AVAILABLE = true
            }
        }
    }
    
    private func formatAddress (parameters : [NSObject : AnyObject]){
        
        self.currentLocation?.current_formattedAddr = (parameters["FormattedAddressLines"] as? [String])?.joinWithSeparator(", ")
        
        guard let city = parameters["City"] as? String else{
            if let city = parameters["SubAdministrativeArea"] as? String{
                self.currentLocation?.current_city = city
            }
            else if let city = parameters["State"] as? String{
                self.currentLocation?.current_city = city
            }
            else if let city = parameters["Country"] as? String{
                self.currentLocation?.current_city = city
            }
            return
        }
        self.currentLocation?.current_city = city
        
    }
    

}
