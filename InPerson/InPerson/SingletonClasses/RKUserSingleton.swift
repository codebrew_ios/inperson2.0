//
//  RKUserSingleton.swift
//  InPerson
//
//  Created by CB Macmini_3 on 17/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation

class RKUserSingleton: NSObject {

    class var sharedInstance: RKUserSingleton {
        struct Static {
            static var instance: RKUserSingleton?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = RKUserSingleton()
        }
        
        return Static.instance!
    }
    
    var loggedInUser : User? {
        get{
            return NSUserDefaults.standardUserDefaults().rm_customObjectForKey("IPloggedInUser") as? User
            //            return User.retrieveSavedLoggedInUser()
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().rm_setCustomObject(value, forKey: "IPloggedInUser")
                //             User.saveLoggedInUser(user: value)
            }else{
                NSUserDefaults.standardUserDefaults().removeObjectForKey("IPloggedInUser")
            }
        }
    }

    var selectedAirport : Airport? {
        get{
            return NSUserDefaults.standardUserDefaults().rm_customObjectForKey("savedAirport") as? Airport
            //            return User.retrieveSavedLoggedInUser()
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().rm_setCustomObject(value, forKey: "savedAirport")
                //             User.saveLoggedInUser(user: value)
            }else{
                NSUserDefaults.standardUserDefaults().removeObjectForKey("savedAirport")
            }
        }
    }
    var deviceToken : String? {
        get{
            return NSUserDefaults.standardUserDefaults().valueForKey("IPDeviceToken") as? String
            //            return User.retrieveSavedLoggedInUser()
        }
        set{
            if let value = newValue {
                NSUserDefaults.standardUserDefaults().setObject(value, forKey: "IPDeviceToken")
                //             User.saveLoggedInUser(user: value)
            }else{
                NSUserDefaults.standardUserDefaults().removeObjectForKey("IPDeviceToken")
            }
        }
    }
}
