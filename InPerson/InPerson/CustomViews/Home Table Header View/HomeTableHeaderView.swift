//
//  HomeTableHeaderView.swift
//  InPerson
//
//  Created by Apple on 09/02/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

protocol HomeTableHeaderViewDelegate {
    
    func searchAirport()
    func relocateUser()
}

class HomeTableHeaderView: UIView {

    var delegate : HomeTableHeaderViewDelegate?
    
    @IBOutlet var imageArrow: UIImageView!
    @IBOutlet var constraintTrailing: NSLayoutConstraint!
    @IBOutlet var constraintLeadingViewSearch: NSLayoutConstraint!
    @IBOutlet var viewSearchHeight: NSLayoutConstraint!
    @IBOutlet var btnSearchBlue: UIButton!
    @IBOutlet var btnAirportName: UIButton!{
        didSet{
            btnAirportName.titleLabel?.adjustsFontSizeToFitWidth = true
        }
    }
    
    @IBOutlet var viewSearch: UIView!{
        didSet{
            viewSearch.layer.cornerRadius = 2.0
            viewSearch.clipsToBounds = true
//            self.roundCorners()
        }
    }
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    var view: UIView!
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "HomeTableHeaderView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }

    @IBAction func actionBtnRelocate(sender: UIButton) {
        
        self.delegate?.relocateUser()
    }
    @IBAction func actionBtnSearch(sender: UIButton) {
    self.delegate?.searchAirport()
    }
    
    override func layoutSubviews() {
//        roundCorners()
        viewSearch.layer.cornerRadius = 2.0
    }
    
    func roundCorners(){
        
        let maskPath = UIBezierPath(roundedRect: self.viewSearch.bounds, byRoundingCorners: [.BottomRight , .TopRight], cornerRadii: CGSize(width: 3.0, height: 3.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.viewSearch.bounds
        maskLayer.path = maskPath.CGPath
        self.viewSearch.layer.mask = maskLayer
        
    }
}
