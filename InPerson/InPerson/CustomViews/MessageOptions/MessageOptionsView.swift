//
//  MessageOptionsView.swift
//  InPerson
//
//  Created by Apple on 02/03/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

protocol MessageOptionsDelegate {
    
    func viewLinkedInProfile()
    func deleteChat()
    func blockUser()
}

class MessageOptionsView: UIView {

    var delegate : MessageOptionsDelegate?
    
    @IBOutlet var btnBlock: UIButton!
    @IBOutlet var imageBlock: UIImageView!
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    var view: UIView!
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "MessageOptions", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }

    
    //MARK: - Button Actions
    
    @IBAction func actionBtnViewLinkedinProfile(sender: UIButton) {
        self.delegate?.viewLinkedInProfile()
    }
    
    @IBAction func actionBtnDeleteChat(sender: UIButton) {
        
        self.delegate?.deleteChat()
    }
    @IBAction func actionBtnBlockUser(sender: UIButton) {
        
        self.delegate?.blockUser()
    }
}
