//
//  SendImageView.swift
//  InPerson
//
//  Created by Apple on 12/02/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

protocol SendImageViewDelegate{
    
    func cancelClicked()
    func sendImageClicked(image : UIImage)
}

class SendImageView: UIView {

    var delegate : SendImageViewDelegate?
    
    @IBOutlet var imageView: UIImageView!
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    var view: UIView!
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "SendImageView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
    @IBAction func actionBtnCancel(sender: UIButton) {
        self.delegate?.cancelClicked()
    }
    
    @IBAction func actionBtnSend(sender: UIButton) {
        
        guard let image = self.imageView.image else { return }
        self.delegate?.sendImageClicked(image)
    }

    @IBAction func actionBtnSaveImage(sender: UIButton) {
        
        guard let image = imageView.image else { return }
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
    }
}
