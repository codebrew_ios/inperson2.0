//
//  SearchCell.swift
//  InPerson
//
//  Created by CB Macmini_3 on 31/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {

    var currentAirport : Airport?{
        didSet{
            if let _ =  currentAirport {
                updateUI()
            }
        }
    }
    
    @IBOutlet var labelAirportName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    
    func updateUI(){
        
        guard let airport = currentAirport,name = airport.airportName,code = airport.airportCode else { return }
        self.labelAirportName.text = code == "" ? name : name + " (" + code + ")"
    }
}
