//
//  ProfileView.swift
//  InPerson
//
//  Created by Apple on 17/02/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit
protocol ProfileViewDelegate {
    
    func dismissProfileView()
    func showLinkedinProfile(userName : String,profileUrl : String)
}

class ProfileView: UIView {

    var delegate : ProfileViewDelegate?
    
    @IBOutlet var overlayView: UIView!
    @IBOutlet var labelHere: UILabel!{
        didSet{
            labelHere.layer.cornerRadius = 4.0
            labelHere.clipsToBounds = true
        }
    }
    @IBOutlet var viewContainer: UIView!{
        didSet{
            viewContainer.layer.cornerRadius = 4.0
            viewContainer.clipsToBounds = true
        }
    }
    @IBOutlet var labelLocation: UILabel!
    @IBOutlet var imageViewUser: UIImageView!
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var labelUserDescription: UILabel!
    @IBOutlet var labelBtnViewProfile: UIButton!{
        didSet{
            labelBtnViewProfile.layer.borderColor = UIColor(hex: 0xE6E6E8).CGColor
            labelBtnViewProfile.layer.cornerRadius = 3.0
            labelBtnViewProfile.layer.borderWidth = 1.0
        }
    }
    
    
    var currentUser : User?{
        didSet{
            updateUI(currentUser)
        }
    }
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    var view: UIView!
    
    func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        
        let tap = UITapGestureRecognizer(target: self, action: Selector("handleTap:"))
        self.overlayView.addGestureRecognizer(tap)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "ProfileView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
    func updateUI(user : User?){
        guard let userName = user?.userName else { return }
        labelLocation.text = user?.userCity ?? ""
        labelUserDescription.text = user?.userJob ?? ""
        labelUserName.text = userName
        let imageUrl = user!.userImageName!
        imageViewUser.sd_setImageWithURL(NSURL(string: imageUrl))
        
        guard let here = user?.userHere else { return }
        labelHere.text = "" + here + "      "
        if here != "Here"{ labelHere.backgroundColor = UIColor(hex: 0x8F8F8F) }
    }

    
    func handleTap(sender : UITapGestureRecognizer){
        
        self.delegate?.dismissProfileView()
    }

    @IBAction func actionBtnViewProfile(sender: UIButton) {
        
        guard let name = currentUser?.userName ,profileUrl = currentUser?.userProfileUrl else { return }
        self.delegate?.showLinkedinProfile(name, profileUrl: profileUrl)
    }
}
