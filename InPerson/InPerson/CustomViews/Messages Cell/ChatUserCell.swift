//
//  ChatUserCell.swift
//  InPerson
//
//  Created by CB Macmini_3 on 08/01/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit
//import MGSwipeTableCell

protocol ChatUserCellDelegate{
    
    func viewOtherUserProfile(userId : String?)
}

class ChatUserCell: MGSwipeTableCell {

    @IBOutlet var constraintDividerHeight: NSLayoutConstraint!
    var chatDelegate : ChatUserCellDelegate?
    @IBOutlet var badgeView: LKBadgeView!{
        didSet{
            badgeView.widthMode = LKBadgeViewWidthModeSmall
            badgeView.badgeColor = UIColor.redColor()
            badgeView.textColor = UIColor.whiteColor()
            badgeView.horizontalAlignment = LKBadgeViewHorizontalAlignmentRight
        }
    }
    @IBOutlet var labelTime: UILabel!
    @IBOutlet var labelLatestMessage: UILabel!
    @IBOutlet var imageUser: UIImageView!{
        didSet{
            imageUser.layer.cornerRadius = imageUser.frame.size.width/2
            imageUser.clipsToBounds = true
        }
    }
    @IBOutlet var labelUserName: UILabel!
    
    var currentChat : Chat?{
        didSet{
            updateUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(){
        constraintDividerHeight.constant = 0.5
        guard let chat = currentChat else { return }
        imageUser.sd_setImageWithURL(NSURL(string: chat.chatImage!), placeholderImage: UIImage(named: "ic_placeholder.png"))
        labelUserName.text = chat.chatName
        labelLatestMessage.text = chat.chatMessage
        labelTime.text = chat.chatTime
        if let unreadCount = chat.chatUnreadCount where Int(unreadCount) != 0 {
            badgeView.text = chat.chatUnreadCount
        }else { badgeView.hidden = true }
    }
    
    @IBAction func actionBtnViewProfile(sender: UIButton) {
        guard let myId = RKUserSingleton.sharedInstance.loggedInUser?.userId else { return }
        self.chatDelegate?.viewOtherUserProfile(myId == self.currentChat?.chatSentBy ? self.currentChat?.chatSentTo : self.currentChat?.chatSentBy)
    }
    
}
