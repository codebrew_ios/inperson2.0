//
//  InviteCell.swift
//  InPerson
//
//  Created by CB Macmini_3 on 08/01/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

protocol RequestDelegate {
    
    func viewProfile(userId : String?)
    func acceptClicked(otherUserId : String,indexPath : NSIndexPath)
    func ignoreClicked(reqId : String,indexPath : NSIndexPath)
}
class InviteCell: UITableViewCell {

    @IBOutlet var constrintDividerHeight: NSLayoutConstraint!{
        didSet{
            constrintDividerHeight.constant = 0.5
        }
    }
    var delegate : RequestDelegate?
    @IBOutlet var btnIgnore: UIButton!{
        didSet{
            btnIgnore.layer.cornerRadius = 2.0
            btnIgnore.clipsToBounds = true
            btnIgnore.layer.borderWidth = 0.5
            btnIgnore.layer.borderColor = UIColor.lightGrayColor().CGColor
        }
    }
    @IBOutlet var btnAccept: UIButton!{
        didSet{
            btnAccept.layer.cornerRadius = 2.0
            btnAccept.clipsToBounds = true
        }
    }
    @IBOutlet var labelTime: UILabel!
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var userImage: UIImageView!{
        didSet{
            userImage.layer.cornerRadius = userImage.frame.size.width/2
            userImage.clipsToBounds = true
        }
    }
    
    var currentRequest : Request?{
        didSet{
            updateUI()
        }
    }
    var indexPath : NSIndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    func updateUI(){
        
        guard let req = currentRequest else { return }
        
        userImage.sd_setImageWithURL(NSURL(string: req.requestUserImage!),placeholderImage: UIImage(named: "ic_placeholder"))
        labelUserName.text = req.requestUserName
        labelTime.text = req.requestTime
    }
    @IBAction func actionBtnIgnore(sender: UIButton) {
        
        guard let _ = self.indexPath?.row, reqId = self.currentRequest?.requestId else { return }
        self.delegate?.ignoreClicked(reqId,indexPath: self.indexPath!)
    }
    
    @IBAction func actionBtnAccept(sender: UIButton) {
        
         guard let _ = self.indexPath?.row, otherUid = self.currentRequest?.user2Id else { return }
        self.delegate?.acceptClicked(otherUid,indexPath: self.indexPath!)
    }
    
    @IBAction func actionBtnProfilePopUp(sender: UIButton) {
        
        self.delegate?.viewProfile(currentRequest?.user2Id)
    }
}
