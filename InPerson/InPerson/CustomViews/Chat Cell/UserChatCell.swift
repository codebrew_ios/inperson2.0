//
//  UserChatCell.swift
//  InPerson
//
//  Created by CB Macmini_3 on 11/01/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit
import PhotosUI

class UserChatCell: UITableViewCell,EasyTipViewDelegate {

    @IBOutlet var constraintTopUserImage: NSLayoutConstraint!
    var saveToolTip : EasyTipView!
    
    var tableView : UITableView?
    @IBOutlet var imageViewUser: UIImageView!{
        didSet{
            imageViewUser.layer.cornerRadius = imageViewUser.frame.size.width / 2
            imageViewUser.clipsToBounds = true
        }
    }
    
    @IBOutlet var labelTime: UILabel!
    @IBOutlet var labelMessage: UILabel!{
        didSet{
            labelMessage.preferredMaxLayoutWidth = UIScreen.mainScreen().bounds.width - 160
        }
    }
    
    @IBOutlet var imageViewChat: UIImageView!{
        didSet{
            imageViewChat.layer.cornerRadius = 12
            imageViewChat.clipsToBounds = true
            
             let longPress = UILongPressGestureRecognizer(target: self, action: Selector("handleLongPress:"))
            longPress.delaysTouchesBegan = true
            longPress.minimumPressDuration = 1.0
            self.addGestureRecognizer(longPress)
        }
    }
    var currentMessage : Message?{
        didSet{
            updateUI()
        }
    }
    
    var previousMessage : Message?{
        didSet{
            
            changeUI()
        }
    }
    var indexPath : NSIndexPath?
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateUI(){
        
        guard let messagetime = self.currentMessage?.messageTime,createdAt = self.currentMessage?.messageCreatedAt, _ = self.currentMessage?.messageDate,userImage = self.currentMessage?.messageImageUrl, imageUrl = NSURL(string: userImage) else { return }
        
         labelTime.text = messagetime
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.dateFromString(createdAt)
        
        let timeInterval = NSDate().timeIntervalSinceDate(date ?? NSDate())
        if timeInterval < 60 {
            labelTime.text = "Just now"
        }

        imageViewUser.sd_setImageWithURL(imageUrl, placeholderImage: UIImage(named: "ic_placeholder")!)
        if let image = self.currentMessage?.messageImage  {
            imageViewChat.image = image
        }
        if let image = self.currentMessage?.messageMedia,imageUrl = NSURL(string: image) where image != "" {
            
            if let image = self.loadImageFromPath(image) {
                
                imageViewChat.image = image
            }else {
                imageViewChat.sd_setImageWithURL(imageUrl, completed: { (imageUpdated, error, cacheType, url) -> Void in
                   
                })
//                imageViewChat.sd_setImageWithURL(imageUrl)
            }
        } else if let message = self.currentMessage?.message {
            self.labelMessage.text = message
        }
    }
    
    func loadImageFromPath(path: String) -> UIImage? {
//        var imagePath = ""
//        do {
//            imagePath = try String(contentsOfURL: path)
//        }catch {
//            
        //        }
        
        let image = UIImage(contentsOfFile: path)
        
        if image == nil {
            
//            print("missing image at: \(path)")
        }
        return image
    }

    
    func changeUI(){
        
        guard let prevMessage = previousMessage,current = currentMessage,chatBubble = self.viewWithTag(9260) as? UIImageView,myId = RKUserSingleton.sharedInstance.loggedInUser?.userId else{
            return
        }
        if prevMessage.messageId == current.messageId {
            imageViewUser.hidden = false
            constraintTopUserImage.constant = 8
            self.updateConstraints()
            
            let imageName = myId == current.messageSentBy ? "ic_chat_bubble_green" : "ic_chat_bubble_blue"
            chatBubble.image =  UIImage(named: imageName)!
        }else if prevMessage.messageSentBy == current.messageSentBy {
            imageViewUser.hidden = true
            let imageName = myId == current.messageSentBy ? "ic_chat_bubble_green-4" : "ic_chat_bubble_blue-3"
            constraintTopUserImage.constant = -2
            self.updateConstraints()
            chatBubble.image =  UIImage(named: imageName)!
        }else if prevMessage.messageSentBy != current.messageSentBy{
            imageViewUser.hidden = false
            constraintTopUserImage.constant = 8
            self.updateConstraints()
            let imageName = myId == current.messageSentBy ? "ic_chat_bubble_green" : "ic_chat_bubble_blue"
            chatBubble.image =  UIImage(named: imageName)!
        }
    }
    
    func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    
    func handleLongPress(sender : UILongPressGestureRecognizer){
        
        if sender.state == .Began {
            var preferences = EasyTipView.Preferences()
            if let _ = saveToolTip {
                saveToolTip.dismiss()
            }
            preferences.drawing.font = UIFont(name: "Futura-Medium", size: 13)!
            preferences.drawing.foregroundColor = UIColor.whiteColor()
            preferences.drawing.backgroundColor = UIColor.blackColor()
            preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.Top
            saveToolTip = EasyTipView(text: "Save",preferences: preferences)
            saveToolTip.show(animated: true, forView: self.viewWithTag(9260)!, withinSuperview: self.tableView)
            
            saveToolTip.delegate = self
        }
    }
    
    func easyTipViewDidDismiss(tipView: EasyTipView) {
        let message = "InPerson"
        
        guard let image = imageViewChat.image,appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate,window = appDelegate.window,chatVC = window.rootViewController else { return }
        let avc = UIActivityViewController(activityItems: [image,message], applicationActivities: nil)
        chatVC.presentViewController(avc, animated: true, completion: nil)
//        var albumPlaceholder:PHObjectPlaceholder!
//        PHPhotoLibrary.sharedPhotoLibrary().performChanges({ () -> Void in
//            guard let image = self.imageViewChat.image else { return }
//            let changeReq = PHAssetChangeRequest.creationRequestForAssetFromImage(image)
//            albumPlaceholder = changeReq.placeholderForCreatedAsset
//            }) { (completed, error) -> Void in
//                
//        }
    }

}
