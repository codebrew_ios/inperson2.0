//
//  InvitationAlert.swift
//  InPerson
//
//  Created by CB Macmini_3 on 30/01/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

protocol InvitationAlertDelegate {
    
    func okayPressed(indexPath : NSIndexPath?)
}

class InvitationAlert: UIView {
    
    
    var delegate : InvitationAlertDelegate?
    
    var indexPath : NSIndexPath?
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
        //        self.addSubview(loaderView)
        //        loader.center = self.center
        //        bringSubviewToFront(loader)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    var view: UIView!
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "showInvitationAlert")
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "InvitationAlert", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }

    @IBAction func actionBtnOkay(sender: UIButton) {
        
        self.delegate?.okayPressed(self.indexPath)
    }

    @IBAction func actionBtnCheck(sender: UIButton) {
        
        sender.selected = !sender.selected
        
        NSUserDefaults.standardUserDefaults().setBool(sender.selected, forKey: "showInvitationAlert")
    }
}
