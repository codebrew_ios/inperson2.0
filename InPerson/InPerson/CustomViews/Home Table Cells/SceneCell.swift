//
//  SceneCell.swift
//  InPerson
//
//  Created by CB Macmini_3 on 24/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation

protocol SceneCellDelegate{
    
    func btnDismissCell(indexPath : NSIndexPath)
}

class SceneCell: UITableViewCell {

    var delegate : SceneCellDelegate?
    
    var indexPath : NSIndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func actionBtnDismissCell(sender: UIButton) {
        
        self.delegate?.btnDismissCell(indexPath)
    }
}
