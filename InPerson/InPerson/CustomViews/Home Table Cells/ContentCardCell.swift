//
//  ContentCardCell.swift
//  InPerson
//
//  Created by Apple on 14/03/16.
//  Copyright © 2016 Code Brew Labs. All rights reserved.
//

import UIKit

class ContentCardCell: UITableViewCell {

    @IBOutlet var containerView: UIView!{
        didSet{
            containerView.layer.cornerRadius = 4.0
            containerView.clipsToBounds = true
            let shadowFrame = self.containerView.layer.bounds
            let shadowPath = UIBezierPath(rect: shadowFrame)
            self.containerView.layer.shadowPath = shadowPath.CGPath
        }
    }
    @IBOutlet var imageViewContent: UIImageView!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelIndicator: UILabel!
    
    var currentContentCard : ContentCard?{
        didSet{
            updateUI()
        }
    }
    
    
    func updateUI(){
    
        guard let card = currentContentCard,image = card.image,url = NSURL(string: image) else { return }

        imageViewContent.sd_setImageWithURL(url)
        labelTitle.text = card.title
        labelIndicator.text = card.category! + "       "
        labelIndicator.layer.cornerRadius = 4.0
        labelIndicator.clipsToBounds = true
    }
}
