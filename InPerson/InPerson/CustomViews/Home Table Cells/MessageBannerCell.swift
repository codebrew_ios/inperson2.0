//
//  MessageBannerCell.swift
//  InPerson
//
//  Created by Apple on 11/03/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

class MessageBannerCell: UITableViewCell {

    @IBOutlet var btnClose: UIButton!
    @IBOutlet var viewBackgound: UIView!{
        didSet{
            viewBackgound.layer.cornerRadius = 4.0
            viewBackgound.clipsToBounds = true
        }
    }
    @IBAction func actionBtnClose(sender: UIButton) {
    }
}
