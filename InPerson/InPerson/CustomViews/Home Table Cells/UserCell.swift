//
//  UserCell.swift
//  InPerson
//
//  Created by CB Macmini_3 on 24/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation
//import MGSwipeTableCell

protocol UserCellDelegate {
    
    func showLinkedinProfile(indexPath : NSIndexPath)
}
class UserCell: MGSwipeTableCell {

    var currentUser : User?{
        didSet{
            updateUI()
        }
    }
    var indexPath : NSIndexPath?
    var delegatelp : UserCellDelegate?
    @IBOutlet var containerView: UIView!
    @IBOutlet var imageViewProfilePic: UIImageView!
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var labelDesigNation: UILabel!
    @IBOutlet var labelPlace: UILabel!
    @IBOutlet var labelIndicator: UILabel!
    @IBOutlet var btnViewLinkedinProfile: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateUI()
    }
    //MARK: - Button Actions
    @IBAction func actionBtnShowLinkedinProfile(sender: UIButton) {
        guard let ip = indexPath else { return }
        delegatelp?.showLinkedinProfile(ip)
    }
    
    func updateUI(){
        
        containerView.layer.cornerRadius = 4.0
        containerView.clipsToBounds = true
        labelIndicator.layer.cornerRadius = 4.0
        btnViewLinkedinProfile.layer.cornerRadius = 3.0
        btnViewLinkedinProfile.layer.borderWidth = 1.0
        btnViewLinkedinProfile.layer.borderColor = UIColor.whiteColor().CGColor
        
        guard let user = currentUser,imageName = user.userImageName else { return }
        labelUserName.text = user.userName
        labelPlace.text = user.userCity
        labelDesigNation.text = user.userJob ?? ""
        
        let imageUrl = imageName 
        imageViewProfilePic.sd_setImageWithURL(NSURL(string: imageUrl))
        
        if user.userId != RKUserSingleton.sharedInstance.loggedInUser?.userId {
            changeUIOtherUser()
            if let f = user.founder where f == "1" {
                labelIndicator.text = "Founder    "
                labelIndicator.backgroundColor = UIColor(hex: 0x0A7EFC)
                return
            }
            guard let here = user.userHere else { return }
            labelIndicator.text = "" + here + "      "
            
            if here != "Here"{ labelIndicator.backgroundColor = UIColor(hex: 0x8F8F8F) }
        }else {
            
            changeUISelf()
            labelIndicator.text = "You      "
        }
    }
    
    func changeUIOtherUser(){
        self.containerView.layer.shadowOpacity = 0.5
        self.containerView.layer.shadowRadius = 15
        self.containerView.layer.shadowOffset = CGSizeMake(0, 0)
        self.containerView.layer.shadowColor = UIColor.blackColor().CGColor
        
        let shadowFrame = self.containerView.layer.bounds
        let shadowPath = UIBezierPath(rect: shadowFrame)
        self.containerView.layer.shadowPath = shadowPath.CGPath
    
        self.containerView.backgroundColor = UIColor.whiteColor()
        self.labelDesigNation.textColor = UIColor.blackColor()
        self.labelPlace.textColor = UIColor.blackColor()
        self.labelUserName.textColor = UIColor.blackColor()
        self.labelIndicator.backgroundColor = UIColor(hex: 0x37C14E)
        self.labelIndicator.textColor = UIColor.whiteColor()
        self.btnViewLinkedinProfile.setTitleColor(UIColor.blackColor(), forState: .Normal)
        self.btnViewLinkedinProfile.setTitle("View LinkedIn Profile", forState: .Normal)
        self.btnViewLinkedinProfile.layer.borderColor = UIColor(hex: 0xE6E6E8).CGColor
        
        
    }
    
    func changeUISelf(){
        
        self.imageViewProfilePic.backgroundColor = UIColor.whiteColor()
        self.containerView.backgroundColor = appColor
        self.labelDesigNation.textColor = UIColor.whiteColor()
        self.labelPlace.textColor = UIColor.whiteColor()
        self.labelUserName.textColor = UIColor.whiteColor()
        self.labelIndicator.backgroundColor = UIColor.whiteColor()
        self.labelIndicator.textColor = appColor
        self.btnViewLinkedinProfile.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.btnViewLinkedinProfile.setTitle("View your LinkedIn Profile", forState: .Normal)
        self.btnViewLinkedinProfile.layer.borderColor = UIColor(red: 1, green: 1.0, blue: 1, alpha: 0.5).CGColor
    }
}
