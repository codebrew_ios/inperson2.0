//
//  RefreshCell.swift
//  InPerson
//
//  Created by Apple on 01/03/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import Foundation

class RefreshCell: UITableViewCell {

    @IBOutlet var btnRefresh: UIButton!{
        didSet{
            btnRefresh.layer.cornerRadius = 4.0
            btnRefresh.layer.borderWidth = 1.0
            btnRefresh.layer.borderColor = UIColor(hex: 0x0A7EFC).CGColor
        }
    }
}
