//
//  Loader.swift
//  Sex Challenge
//
//  Created by CB Macmini_3 on 06/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation

class Loader: UIView {

    @IBOutlet var loaderView: UIView!
    @IBOutlet weak var overlay: UIView!
    
    @IBOutlet var constraintCenterVertically: NSLayoutConstraint!
    @IBOutlet var labelLoadingText: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        xibSetup()
//        self.addSubview(loaderView)
//        loader.center = self.center
//        bringSubviewToFront(loader)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func startAnimating(){
        
        activityIndicator.startAnimating()
//        loader.startAnimating()
    }
    
    func stopAnimating(){
        activityIndicator.stopAnimating()
//        loader.stopAnimating()
    }
    
    var view: UIView!
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "Loader", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
   
}
