//
//  AppDelegateExtension.swift
//  InPerson
//
//  Created by Apple on 09/03/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import Foundation
import SwiftyJSON


extension AppDelegate {
    
    
    //MARK: - Push Notifications
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        let deviceToken = deviceToken.description.stringByReplacingOccurrencesOfString("<", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString(">", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        RKUserSingleton.sharedInstance.deviceToken = deviceToken
        User.registerDeviceToken()
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        NSNotificationCenter.defaultCenter().postNotificationName("IPPush", object: nil, userInfo: userInfo)
    }

    
    func logout(){
        
        RKUserSingleton.sharedInstance.loggedInUser = nil
        
        let main = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        
        guard let VC = main.instantiateViewControllerWithIdentifier("SignInViewController") as? SignInViewController else { return }
        VC.firstTime = false
        let navigationController = CustomNavigationController(rootViewController : VC)
        
        UIView.transitionWithView(self.window!, duration: 0.5, options: .TransitionFlipFromLeft , animations: { () -> Void in
            
            self.window!.rootViewController = navigationController
            }) { (completed) -> Void in
                
                self.showNativeAlert("Please Login to continue.")
        }
    }
    func logoutWithoutAlert(){
        
        
        RKUserSingleton.sharedInstance.loggedInUser = nil
        
        let main = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        
        guard let VC = main.instantiateViewControllerWithIdentifier("SignInViewController") as? SignInViewController else { return }
        VC.firstTime = false
        let navigationController = CustomNavigationController(rootViewController : VC)
        
        UIView.transitionWithView(self.window!, duration: 0.5, options: .TransitionFlipFromLeft , animations: { () -> Void in
            
            self.window!.rootViewController = navigationController
            }) { (completed) -> Void in
                
                //                self.showNativeAlert("Please Login to continue.")
        }
    }
    
    func showNativeAlert(message : String){
        
        let controller = UIAlertController(title: nil, message: message , preferredStyle: UIAlertControllerStyle.Alert)
        let yes = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in }
        controller.addAction(yes)
        self.window?.rootViewController?.presentViewController(controller, animated: true, completion: nil)
    }

}