//
//  BaseChatViewController.swift
//  Whatashadi
//
//  Created by Night Reaper on 19/12/15.
//  Copyright © 2015 Gagan. All rights reserved.
//

import UIKit
import PhotoSlider
import SwiftyJSON

struct CellType {
    static let TextCell = "0"
    static let ImageCell = "1"
}


extension UITableView {
    func reloadData(completion: ()->()) {
        UIView.animateWithDuration(0, animations: { self.reloadData() })
            { _ in completion() }
    }
}


class BaseChatViewController: RKViewController , UIScrollViewDelegate {

    @IBOutlet var labelPlaceHolder: UILabel!
    @IBOutlet var constraint_view_bottom: NSLayoutConstraint!
    
    
    @IBOutlet var tv_chat: UITextView!{
        didSet{
            tv_chat?.placeholder = "Write Message"
            tv_chat.layer.cornerRadius = 4.0
            tv_chat.clipsToBounds = true
        }
    }
    @IBOutlet var tableView: UITableView!{
        didSet{
            
//            self.tableView.addSubview(self.refreshControl)
        }
    }
    
    var tabbarHeight : CGFloat = 0.0
    var DELAY = 0.0
    var myImageUrl : String?

    var dbManager = ChatDBManager()
    var array_messages : [Message] = []
    var tableViewDataSoure : ChatDataSource = ChatDataSource()
    var kbHeight : CGFloat = 216.0
    private var pollingTimer : NSTimer?
    let userAccessToken = RKUserSingleton.sharedInstance.loggedInUser?.userToken ?? "0"
    let myId = RKUserSingleton.sharedInstance.loggedInUser?.userId ?? ""
    var lastMessageId : String?
    var otherUserId : String?
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: "handleRefresh:", forControlEvents: UIControlEvents.ValueChanged)
        return refreshControl
    }()

    
    var chatMessage : Message? {
        didSet{
            if let current = chatMessage{
                self.tv_chat.text = ""
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    weak var weak : BaseChatViewController? = self
                    weak?.tableView?.beginUpdates()
                    self.array_messages.append(current)
                    self.tableViewDataSoure.messages?.append(current)
                    weak?.tableView?.insertRowsAtIndexPaths([NSIndexPath(forRow: (weak?.array_messages.count ?? 0) - 1, inSection: 0)], withRowAnimation: UITableViewRowAnimation.None)
                    weak?.tableView?.endUpdates()
                    weak?.scrollToBottom(animate: true)
                })
            }
        }
    }
    

    
    //MARK: - View Hierarchy Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTableViewCell()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpKeyboardNotifications()
    }
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.invalidateTimer()
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    //MARK: - Paging
    func handleRefresh(sender : UIRefreshControl){
     
        sender.endRefreshing()
    }

    
    //MARK: - Polling
    func startPolling(){
        self.pollingTimer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: "polling", userInfo: nil, repeats: true)
    }
    
    func invalidateTimer(){
        self.pollingTimer?.invalidate()
        self.pollingTimer = nil
    }
    
    func polling(){
        self.tableView.reloadData()
       self.calculateLastMessageId(self.array_messages ?? [])
        guard let lastmsgId = self.lastMessageId, userId = RKUserSingleton.sharedInstance.loggedInUser?.userId,otherId = otherUserId else {
            self.invalidateTimer()
            return }
        let timeZOne = NSTimeZone.localTimeZone()
        let zoneName = timeZOne.name
        let url = URLS.BASE + userId + "/message/" + lastmsgId
        
        webServiceForPolling(params: ["access_token" : self.userAccessToken,"zone" : zoneName,"id" : otherId], url: url) { (messages) -> () in
        }
    }
    
    
    func webServiceForPolling(params params : [String : String ] , url : String , completeion : ([AnyObject]?)-> ()){
        
        Message.pollOtherUserMessages(params, url: url, success: { (messages) -> () in
            
            weak var weak : BaseChatViewController? = self
            guard let results = messages as [Message]? else{
                return
            }
            weak?.appendNewMessages(messages: results)
            completeion(weak?.array_messages)
            weak?.saveRecievedMessages(messages)
            
//            for message in messages {
//                if let imageUrl = message.messageMedia where imageUrl != "" {
//                    let url = NSURL(string: imageUrl)
//                    let data = NSData(contentsOfURL: url!)
//                     UIImageWriteToSavedPhotosAlbum(UIImage(data: data!)!, nil, nil, nil)
//                }
//              
//            }
            }, failure: { (error) -> () in
                
                
            }) { () -> () in
                super.logout()
                self.invalidateTimer()
        }
    }
    
    
    func saveRecievedMessages(msgs : [Message]){
        
        for message in msgs {
            dbManager.saveMessage(message)
        }
    }
    

    //MARK: - WebService and tableView Configuration
    
    func configureTableViewCell(){
        weak var weak : BaseChatViewController? = self
        tableViewDataSoure = ChatDataSource(items: self.array_messages, tableView: self.tableView, configureCellBlock: { (cell, item,prevItem,indexPath) -> () in
            guard let tempCell = cell as? UserChatCell , modal = item as? Message,prevModal = prevItem as? Message else { return }
            tempCell.currentMessage = modal
            tempCell.previousMessage = prevModal
            tempCell.indexPath = indexPath
            tempCell.tableView = self.tableView
            }, aRowSelectedListener: { (indexPath) -> () in
                
                self.view.endEditing(true)
                weak?.cellDidClicked(indexPath: indexPath)
        })
        
        tableView.dataSource = tableViewDataSoure
        tableView.delegate = tableViewDataSoure
  
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            weak?.tableView?.reloadData()
        })
    }
 
    func cellDidClicked(indexPath indexPath: NSIndexPath) {
        let tempModal = self.array_messages[indexPath.row]
        let sc = tempModal.messageMedia! != "" ? "1" : "0"
        switch sc{
        case CellType.ImageCell:
            self.imageClicked(tempModal)
            break
        default : break
        }
    }
    
    
    //MARK: - Images Clicked Delegate
    func imageClicked(modal: Message?) {
        guard let imageStr = modal?.messageMedia , imageUrl = NSURL(string: imageStr) else {
            return
        }
        var slider = PhotoSlider.ViewController(images: [])
        if imageStr.containsString("http") {
            slider = PhotoSlider.ViewController(imageURLs: [imageUrl])
        }else{
            if let image = UIImage(contentsOfFile: imageStr){
                slider = PhotoSlider.ViewController(images: [image])
            }else {
                return
            }
        }
        
        slider.modalPresentationStyle = .OverCurrentContext
        slider.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
        guard let tabbar = self.tabBarController else {
            self.presentViewController(slider, animated: true, completion: nil)
            return
        }
        tabbar.presentViewController(slider, animated: true, completion: nil)
    }
    
    
    func photoSliderControllerDidDismiss(viewController: ViewController) {
        
    }
    func photoSliderControllerWillDismiss(viewController: ViewController) {
        
    }
    
    func getAllMessages(params params : [String : String] , url : String ,success : () -> ()){
         weak var weak : BaseChatViewController? = self
        
        Message.viewAllMessages(params, url: url, success: { (messages) -> () in
            
            weak?.array_messages = messages
            weak?.tableViewDataSoure.messages = weak?.array_messages
            if self.array_messages.count != 0 {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    weak?.tableView?.reloadData()
                    weak?.scrollToBottom(animate: true)
                })
            }
            weak?.calculateLastMessageId(weak?.array_messages ?? [])
            weak?.saveLatestMessages(messages, oid: self.otherUserId!)
            success()
            }, failure: { (error) -> () in
                
                weak?.startPolling()
                super.showNativeAlert(error)
            }) { () -> () in
               super.logout()
        }
    }
    
    func getAllMessagesFromDatabase(success: ([Message]) -> ()){
        
        guard let oid = otherUserId else { return }
        self.array_messages = dbManager.retrieveSavedLoggedInMessages(oid)
        
        success(array_messages)
        self.tableViewDataSoure.messages = self.array_messages
        self.tableView?.reloadData()
        self.scrollToBottom(animate: false)
        self.calculateLastMessageId(self.array_messages ?? [])
        polling()
        self.startPolling()
    }
    
    func saveLatestMessages(messages : [Message],oid : String){
        let savedMessages = dbManager.retrieveSavedLoggedInMessages(oid)
        guard let lastSavedMessage = savedMessages.last, latestMessage = messages.last, lastmsgId = Int(lastSavedMessage.messageId!),latestMsgId = Int(latestMessage.messageId!) else {
            
            for msg in messages{
                self.dbManager.saveMessage(msg)
            }
            return
        }
        
        if lastmsgId >= latestMsgId { return }else if lastmsgId < latestMsgId {
            
            for (smsg,lmsg) in zip(savedMessages, messages){
                 print(smsg.messageId! + "\(lmsg.messageId)")
            }
            for var i = 0 ; i < messages.count - 1 ; i++ {
                if i >= savedMessages.count{
                    dbManager.saveMessage(messages[i])
                }else if savedMessages[i].messageId == messages[i].messageId{ continue }else{
                    
                    dbManager.saveMessage(messages[i])
                }
            }
        }
        self.getAllMessagesFromDatabase { (messages) -> () in
        }
    }
    
    func sendMessageHandler (type type : String , message : String? , image : UIImage?){
        
        let params = self.formatParameters(type, message: message)
        let msgs = self.array_messages
        guard let lastMsgId = Int(msgs.last?.messageId ?? "1") else { return }
        
        if let _ = image {
            self.saveSentMessage("\(lastMsgId + 1)", imageUrl: dbManager.saveImageToDocumentsDirectory(image!, userID: self.otherUserId!), message: message ?? "")
        }else {
            self.saveSentMessage("\(lastMsgId + 1)", imageUrl: nil, message: message!)
        }
        
        let url = URLS.BASE + myId + "/send/" + otherUserId!
        
        Message.sendMessage(params, image: (type == CellType.ImageCell ? [image ?? UIImage()] : nil), url: url, success: { (msgID,imageUrl) -> () in
            
            print(msgID)
            }, failure: { (error) -> () in
                
                
            }) { () -> () in
                
              super.logout()
        }
    }
    
    
    func formatParameters (type : String , message : String?) -> [String : String]{
        
        guard let currentMsgId = Int(lastMessageId ?? "1"),imageUrl = RKUserSingleton.sharedInstance.loggedInUser?.userImage else { return ["":""] }
        
        var params = [ "access_token" : userAccessToken, "senderid" : myId,"recieverid" : otherUserId ?? "","time" : RKLocationManager.sharedInstance.currentTime?.currentTime ?? "just now","date" : RKLocationManager.sharedInstance.currentTime?.currentDate ?? "","id" : "\(currentMsgId)","sender_imageurl" : imageUrl]
        
        
        if let text = message where type == CellType.TextCell {
            params["message"] = text
        }
        return params
    }
    
    func saveSentMessage (messageId : String?,imageUrl : String?,message : String) {
        
        guard let _ = Int(lastMessageId ?? "1"),userImageUrl = RKUserSingleton.sharedInstance.loggedInUser?.userImage else { return }
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var params = ["sent_by" : myId,"sent_to" : otherUserId ?? "","time" : RKLocationManager.sharedInstance.currentTime?.currentTime ?? "Just now","date" : RKLocationManager.sharedInstance.currentTime?.currentDate ?? "","id" : messageId ?? "1","sender_imageurl" : userImageUrl,"created_at" : dateFormatter.stringFromDate(NSDate())]
        
        if let imgUrl = imageUrl{
        
            params["media"] = imgUrl
            params["message"] = ""
        }else{
             params["message"] = message
            params["media"] = ""
        }
      
      let msg = Message(attributes: params)
        self.dbManager.saveMessage(msg)
        chatMessage = msg
    }

    //MARK: -  Keyboard Notifications
    
    func setUpKeyboardNotifications(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: self.view?.window)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: self.view?.window)
    }
    
    func keyboardWillShow(n : NSNotification){
        if let userInfo = n.userInfo{
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue)
            let rect = keyboardFrame.CGRectValue()
            kbHeight = CGFloat(rect.height) - tabbarHeight
            self.setOffset(hide: false)
        }
        self.labelPlaceHolder.hidden = true
    }
    func keyboardWillHide (n : NSNotification){
        self.setOffset(hide: true)
        self.labelPlaceHolder.hidden = self.array_messages.count > 0 ? true : false
    }
    
    func setOffset (hide hide : Bool){
        if (hide)
        {
            self.constraint_view_bottom?.constant = 0
            UIView.animateWithDuration(0.25, animations: { () -> Void in
                self.view.layoutIfNeeded()
                }, completion: { (animated) -> Void in
            })
        }
        else
        {
            self.constraint_view_bottom?.constant = kbHeight ;
            UIView.animateWithDuration(0.25, animations: { () -> Void in
                self.view.layoutIfNeeded()
                }, completion: { (animated) -> Void in
                    self.scrollToLastestSeenMessageAnimated(animated: true)
            })
        }
        keyboardToggle(hide)
    }
    
    func keyboardToggle(hide : Bool){
        
    }
    
    func scrollToLastestSeenMessageAnimated(animated animated : Bool){
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            let bottomOffset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height)
            if bottomOffset.y > 0{
//                print(bottomOffset.y)
                self.tableView.setContentOffset(bottomOffset, animated: animated)
            }
        }
    }
    
    
    
    func scrollToBottom(animate animate : Bool){
        if self.array_messages.count > 0{
            self.delay(DELAY , closure: { () -> () in
                self.tableView?.scrollToRowAtIndexPath(NSIndexPath(forRow: self.array_messages.count - 1, inSection: 0), atScrollPosition: .Top, animated: animate)
            })
        }
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    
    //MARK: - Duplicate Message Handler
    
    func appendNewMessages(messages messages : [Message]){
        if messages.count > 0 {
            self.removeDuplicateMessages(messages: messages)
        }
    }
    
    func removeDuplicateMessages (messages messages : [Message]){
        var indexPaths : [NSIndexPath] = []
        for newMessage in messages{
            var FLAG = true
            for oldMessage in self.array_messages {
                if let nm_id = newMessage.messageId , om_id = oldMessage.messageId {
                    if nm_id == om_id{
                        FLAG = false
                        break
                    }
                    FLAG = true
                }
            }
            if FLAG   {
                self.array_messages.append(newMessage)
                tableViewDataSoure.messages?.append(newMessage)
                self.calculateLastMessageId(self.array_messages)
                indexPaths.append(NSIndexPath(forRow: self.array_messages.count - 1, inSection: 0))
            }
        }
        if indexPaths.count > 0{
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                weak var weak : BaseChatViewController? = self
                weak?.tableView?.beginUpdates()
                weak?.tableView?.insertRowsAtIndexPaths(indexPaths , withRowAnimation: .Fade)
                weak?.tableView?.endUpdates()
                weak?.scrollToBottom(animate: true)
            })
        }
    }
    

    func calculateLastMessageId (messages : [Message]){
                if messages.count > 0{
                    guard let lastMsgId = messages.last?.messageId else{ return  }
                    self.lastMessageId = lastMsgId
                }
        }

}
