//
//  SignInViewController.swift
//  InPerson
//
//  Created by CB Macmini_3 on 17/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import UIKit
//import IOSLinkedInAPI
import SwiftyJSON


class SignInViewController: RKViewController {

    @IBOutlet var labelScene: UILabel!
    @IBOutlet var logo: UIImageView!
    @IBOutlet var ImageScene: UIButton!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var viewLogo: UIView!
    
    var firstTime : Bool = true
    var client = LIALinkedInHttpClient()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        client = linkedInClient()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if firstTime {
        logo.center = CGPointMake(self.view.center.x,self.view.center.y)
        ImageScene.transform = CGAffineTransformMakeTranslation(0, UIScreen.mainScreen().bounds.size.height )
        btnLogin.transform = CGAffineTransformMakeTranslation(0, UIScreen.mainScreen().bounds.size.height - 64)
            labelScene.transform = CGAffineTransformMakeTranslation(0, UIScreen.mainScreen().bounds.size.height + 20)
            animations()
            firstTime = false
        }
    }
    
    
    func animations(){
        
        UIView.animateWithDuration(0.35, delay: 0.2, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            
            self.logo.center = self.viewLogo.center
            }) { (completed) -> Void in
        }
        UIView.animateWithDuration(0.5) { () -> Void in
            
            self.ImageScene.transform = CGAffineTransformIdentity
            self.btnLogin.transform = CGAffineTransformIdentity
            self.labelScene.transform = CGAffineTransformIdentity
        }
    }
    
    //MARK: - Linked In Login
    
    func linkedInClient() -> LIALinkedInHttpClient{
        let application = LIALinkedInApplication(redirectURL: LinkedInC.LinkedIn_Redirect_url , clientId: LinkedInC.LinkedIn_ClientId , clientSecret: LinkedInC.LinkedIn_ClientSecret, state: LinkedInC.LinkedIn_state, grantedAccess: LinkedInC.LinkedIn_Permissions )
        return LIALinkedInHttpClient(forApplication: application, presentingViewController: nil)
    }
    
    func loginViaLinkedIn(){
        self.client.getAuthorizationCode({ (code) -> Void in
            self.client.getAccessToken(code, success: { (accesstokenData) -> Void in
                
                self.fetchLinkedInInfo(accesstokenData["access_token"] as? String)
                
                }, failure: { (error) -> Void in
                    super.hideLoader()
            })
            
            }, cancel: { () -> Void in
                super.hideLoader()
                
            }) { (error) -> Void in
                super.hideLoader()
                super.showNativeAlert(error.localizedDescription)
        }
        
    }
    
    
    func fetchLinkedInInfo(accesstokenData : String?){
        if let token = accesstokenData{
            LinkedIn.linkedInInfo(token, success: { (linkedInUser) -> () in
                
                self.postLinkedInDataToServer(linkedInUser , accessToken :token)
                
                }, failure: { () -> () in
                    super.hideLoader()
            })
        }
    }
    
    
    func postLinkedInDataToServer(linkedInUser : LinkedIn? , accessToken : String?){
        
        if let user = linkedInUser,location = RKLocationManager.sharedInstance.currentLocation {

            let parameters = ["linkedin_id" : user.linkedin_id ?? ""  , "email" :  user.linkedin_email ?? ""  , "name" :  user.linkedin_name ?? "" , "lat" : location.current_lat ?? "0" ,"lng" : location.current_lng ?? "0","job" : user.linkedin_headline ?? ""  , "company" :  user.linkedin_company ?? ""  , "city" :  user.linkedin_location ?? "","profile_url" : user.linkedin_profileUrl!,"otherdata": ""]
            
            
            LinkedIn.userLinkedInLogin(parameters ,image: user.linkedin_pic ?? "", success: { (user) -> () in
                RKUserSingleton.sharedInstance.loggedInUser = user
                self.handleLoginResponse()
                super.hideLoader()
                }, failure: { (error) -> () in
                    super.hideLoader()
                    super.showNativeAlert(error)
            })
            
            
        }
    }
    
    @IBAction func actionbtnLinkedinlogin(sender: UIButton) {
        
        if UIApplication.sharedApplication().canOpenURL(NSURL(string: "linkedin://")!){
            
            LISDKSessionManager.createSessionWithAuth([LISDK_BASIC_PROFILE_PERMISSION,LISDK_EMAILADDRESS_PERMISSION], state: nil, showGoToAppStoreDialog: true, successBlock: { (response) -> Void in
                
                let session = LISDKSessionManager.sharedInstance().session
                
                print(session.value() + "\(session.isValid())")
                
                let url = "https://api.linkedin.com/v1/people/~:(id,formatted-name,num-connections,picture-url,email-address,specialties,summary,location:(name),industry,network,site-standard-profile-request,public-profile-url,picture-urls::(original),skills,positions,educations,headline)"
                self.showLoader()
                if LISDKSessionManager.hasValidSession() {
                    
                    LISDKAPIHelper.sharedInstance().getRequest(url, success: { (response) -> Void in
                        
                        let string = JSON(response.data)
                        let data = string.rawString()?.dataUsingEncoding(NSUTF8StringEncoding)
                        do {
                        let dict = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers)
                           let json = JSON(dict)
                            let user = LinkedIn(attributes: json.dictionaryValue)
                            
                            self.postLinkedInDataToServer(user, accessToken: "")
                        }catch {
                            
                        }
                        }, error: { (error) -> Void in
                            
                            
                    })
                }
                
                }, errorBlock: { (error) -> Void in
                    
                  super.showNativeAlert("Please check your Internet Connection.")
            })
            
        }else{

            showLoader()
            self.loginViaLinkedIn()
        }

//        handleLoginResponse()
    }
  
    func handleLoginResponse(){
        
        let VC = self.storyboard?.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
        VC.comingFirstTime = true
        if let _ = NSUserDefaults.standardUserDefaults().valueForKey("IPShowcard") {
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "IPShowcard")
        }else{
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "IPShowcard")
        }
        
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func unwindToSignIn(segue: UIStoryboardSegue) {
        
        RKUserSingleton.sharedInstance.loggedInUser = nil
    }
}
