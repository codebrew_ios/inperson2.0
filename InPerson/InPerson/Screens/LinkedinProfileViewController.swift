//
//  LinkedinProfileViewController.swift
//  InPerson
//
//  Created by CB Macmini_3 on 18/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation

class LinkedinProfileViewController: UIViewController {

    
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var webView: UIWebView!
    
    var userName : String?
    var linkedinProfileUrl : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        guard let strUrl = linkedinProfileUrl, name = userName else { return }
        labelTitle.text = name
        let url = NSURL (string: strUrl)
        let requestObj = NSURLRequest(URL: url!);
        webView.loadRequest(requestObj)
    }
    
    
    @IBAction func actionBtnClose(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
//    https://www.linkedin.com/in/nitin-malik-2a515015
}
