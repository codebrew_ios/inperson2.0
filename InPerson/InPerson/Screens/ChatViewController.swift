//
//  ChatViewController.swift
//  InPerson
//
//  Created by CB Macmini_3 on 11/01/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

class ChatViewController: BaseChatViewController,SendImageViewDelegate,MessageOptionsDelegate {

    @IBOutlet var labelUserName: UIButton!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnDropDown: UIButton!
    @IBOutlet var viewNavigationBar: UIView!
    var sendImageView = SendImageView()
    var messageOptionsView = MessageOptionsView()
    @IBOutlet var viewChat: UIView!
    
    var imageUrl : String?
    
    var placeholderHidden : Bool = true
    var userId2 : String?{
        didSet{
            super.otherUserId = userId2
        }
    }
    
    var otherUserProfileUrl : String?
    var otherUserName : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewNavigationBar.backgroundColor = UIColor.whiteColor()
        setupMessageOptionsView()
        labelPlaceHolder.hidden = placeholderHidden
       setupSendImageView()
        labelUserName.setTitle(otherUserName ?? "", forState: .Normal)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        super.getAllMessagesFromDatabase { (messages) -> () in
            
            self.labelPlaceHolder.hidden = messages.count > 0 ? true : false
        }
         getMessages()
        
    }
    
    func getMessages(){
        
        guard let otheruserId = userId2,myId = RKUserSingleton.sharedInstance.loggedInUser?.userId,token = RKUserSingleton.sharedInstance.loggedInUser?.userToken else { return }
        
        let url = URLS.BASE + myId + "/viewmsg/" + otheruserId
        let timeZone = NSTimeZone.localTimeZone()
        let zoneName = timeZone.name
        if super.array_messages.count == 0 {
            getAllMessages(params: ["access_token" : token,"zone":zoneName], url: url) { () -> () in
                self.labelPlaceHolder.hidden = self.array_messages.count > 0 ? true : false
            }
        }
        
    }
    

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func actionBtnBack(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
//        self.performSegueWithIdentifier("ChatToMessages", sender: self)
    }
    @IBAction func actionBtnCamera(sender: UIButton) {
        self.view.endEditing(true)
        
         self.addMorePictures("photo library")
    }
    
    private func addMorePictures (pickerType : String){
        
        showActionSheet(["Gallery","Camera"], title: "")
    }
    @IBAction func actionBtnSend(sender: UIButton) {
        let text = self.tv_chat.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if text == "" { return }
        self.labelPlaceHolder.hidden = true
        super.sendMessageHandler(type: CellType.TextCell, message: text, image: nil)
        
    }
    
    @IBAction func actionBtnOptions(sender: UIButton) {
        sender.selected = !sender.selected
        labelUserName.selected = sender.selected
        if sender.selected {
            showMessageOptionsView()
        }else {
            hideMessageOptionsView()
        }
    }
    
    func showActionSheet(buttons : [String] , title : String){
        
        let alertController = UIAlertController.showActionSheetController(title: "Photo Source", buttons: buttons,success: { (msg) -> () in
            unowned let _ : ChatViewController = self
            
            if msg == "Gallery"{
                CameraGalleryPickerBlock.sharedInstance.pickerImage(type: "Photo Library" , presentInVc: self, pickedListner: { (image,imageUrl) -> () in
                    weak var weak : ChatViewController? = self
                    weak?.imageUrl = imageUrl
                    weak?.showSendImageView(image)
                    
                    // Send Image Here
                    }) { () -> () in
                }
            }else {
                
                CameraGalleryPickerBlock.sharedInstance.pickerImage(type: "Camera" , presentInVc: self, pickedListner: { (image,imageUrl) -> () in
                    weak var weak : ChatViewController? = self
                    dispatch_async(dispatch_get_main_queue(),{
                        
                        weak?.showSendImageView(image)
                        weak?.imageUrl = imageUrl
                        
                    })
                    // Send Image Here
                    }) { () -> () in
                }
            }
        })
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    //MARK: - Send Image View Methods
    func setupSendImageView(){
        
        sendImageView = SendImageView(frame: UIScreen.mainScreen().bounds)
        sendImageView.delegate = self
    }
    
    func showSendImageView(image : UIImage){
        self.view.addSubview(sendImageView)
        sendImageView.imageView.image = image
      
        sendImageView.transform = CGAffineTransformMakeTranslation(0, -UIScreen.mainScreen().bounds.height)
        UIView.animateWithDuration(0.3) { () -> Void in
            
            self.sendImageView.transform = CGAffineTransformMakeTranslation(0, 0)
        }
    }
    
    func hideSendImageView(){
        
        sendImageView.transform = CGAffineTransformMakeTranslation(0, 0)
        
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            
            self.sendImageView.transform = CGAffineTransformMakeTranslation(0, -UIScreen.mainScreen().bounds.height)

            }) { (completed) -> Void in
                
               self.sendImageView.removeFromSuperview()
        }
    }
    
    func cancelClicked() {
        self.hideSendImageView()
    }
    
    func sendImageClicked(image: UIImage) {
        hideSendImageView()
        self.labelPlaceHolder.hidden = true
        guard let imgUrl = self.imageUrl else { return }
        self.sendMessageHandler(type: "1", message: imgUrl, image: image)
    }
    
    //MARK: - Message Options View Methods
    
    func setupMessageOptionsView(){
        
        messageOptionsView = MessageOptionsView(frame: CGRect(x: 0, y: 64, width: UIScreen.mainScreen().bounds.size.width, height: otherUserName == "Team InPerson" ? 104 : 164))
        messageOptionsView.btnBlock.hidden = otherUserName == "Team InPerson" ? true : false
        messageOptionsView.imageBlock.hidden = otherUserName == "Team InPerson" ? true : false
        messageOptionsView.delegate = self
    }
    
    func showMessageOptionsView(){
        
        self.view.addSubview(messageOptionsView)
        self.view.bringSubviewToFront(viewNavigationBar)
        messageOptionsView.transform = CGAffineTransformMakeTranslation(0, -164)
        messageOptionsView.backgroundColor = UIColor.whiteColor()
        btnBack.setImage(UIImage(named: "ic_back_white"), forState: .Normal)
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.messageOptionsView.transform = CGAffineTransformMakeTranslation(0, 0)
            self.viewNavigationBar.backgroundColor = UIColor(hex: 0x0A7EFC)
            self.messageOptionsView.backgroundColor = UIColor(hex: 0x0A7EFC)
            }) { (completed) -> Void in
                
        }
    }
    
    func hideMessageOptionsView(){
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.messageOptionsView.transform = CGAffineTransformMakeTranslation(0, -164)
            self.viewNavigationBar.backgroundColor = UIColor.whiteColor()
            }) { (completed) -> Void in
                self.btnBack.setImage(UIImage(named: "ic_back"), forState: .Normal)
                self.messageOptionsView.removeFromSuperview()
        }
    }
    
    func viewLinkedInProfile() {
        
        guard let id = userId2,myId = RKUserSingleton.sharedInstance.loggedInUser?.userId,token = RKUserSingleton.sharedInstance.loggedInUser?.userToken else { return }
        showLoader()
        User.viewOtherUserProfile(myId, otherId: id, token: token, success: { (user) -> () in
            self.viewProfileHandler(user)
            super.hideLoader()
            }, failure: { (error) -> () in
                super.showNativeAlert(error)
                super.hideLoader()
            }) { () -> () in
                super.hideLoader()
                super.logout()
        }
    }
    
    func viewProfileHandler(user : User){
        
        
        guard let VC2 = self.storyboard?.instantiateViewControllerWithIdentifier("LinkedinProfileViewController") as? LinkedinProfileViewController,me = RKUserSingleton.sharedInstance.loggedInUser,profileUrl = me.userProfileUrl,name = me.userName,id = me.userId,otherId = userId2 else { return }
        
        if id == otherId {
            VC2.linkedinProfileUrl = profileUrl
            VC2.userName = name
        }else {
            guard let profileUrl = user.userProfileUrl else { return }
            VC2.linkedinProfileUrl = profileUrl
            VC2.userName = user.userName
        }
        
        
        self.presentViewController(VC2, animated: true, completion: nil)
    }
    
    func blockUser() {
        
        super.showNativeAlert("Blocking user will block all interactions.", message: "", buttonTitle: "Block", cancel: { () -> () in
            
            }) { () -> () in
                
                guard let id = self.userId2 else { return }
                super.showLoader()
                User.blockOtherUser(id, success: { () -> () in
                    
                    super.hideLoader()
                    
                    self.showNativeAlert("User is now blocked.", buttonTitle: "Close")
                    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
                    dispatch_after(delayTime, dispatch_get_main_queue()) {
                        
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                    
                    }, failure: { (error) -> () in
                        super.hideLoader()
                        super.showNativeAlert(error)
                    }) { () -> () in
                        super.hideLoader()
                        super.logout()
                }
        }
        
    }
    
    func deleteChat() {
        
        guard let userId = RKUserSingleton.sharedInstance.loggedInUser?.userId,token = RKUserSingleton.sharedInstance.loggedInUser?.userToken, id = userId2 else { return }
        ChatDBManager.deleteMessages(id)
        showLoader()
        Message.deleteAllMessages(userId, otherUId: id, token: token, success: { () -> () in
            super.hideLoader()
            self.navigationController?.popViewControllerAnimated(true)
            }, failure: { (error) -> () in
                super.hideLoader()
                
            }) { () -> () in
                super.hideLoader()
                super.logout()
        }
        
    }
    
    override func keyboardToggle(hide : Bool) {
        
        if !hide && viewNavigationBar.backgroundColor != UIColor.whiteColor(){
            btnDropDown.selected = !btnDropDown.selected
            labelUserName.selected = btnDropDown.selected
            hideMessageOptionsView()
        }
    }
    
    override func handlePush(notification: NSNotification) {
        polling()
    }
    
//    override func pushNotificationHandler(push: Push) {
//        
//        polling()
//    }
}
