//
//  ContentDescriptionViewController.swift
//  InPerson
//
//  Created by Apple on 14/03/16.
//  Copyright © 2016 Code Brew Labs. All rights reserved.
//

import UIKit

class ContentDescriptionViewController: UIViewController {

    @IBOutlet var imageViewContent: UIImageView!
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelCategory: UILabel!
    
    var currentCard : ContentCard?
    let transition = ExpandingCellTransition()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transitioningDelegate = transition
        guard let card = currentCard else { return }
        
        imageViewContent.sd_setImageWithURL(NSURL(string: card.image!))
        labelCategory.text = card.category
        labelTitle.text = card.title
    }
    @IBAction func actionBtnClose(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
