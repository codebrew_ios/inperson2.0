//
//  LaunchScreenViewController.swift
//  InPerson
//
//  Created by CB Macmini_3 on 17/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import UIKit

class LaunchScreenViewController: RKViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let userToken = RKUserSingleton.sharedInstance.loggedInUser?.userToken where userToken != "" {
            
            //            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
            
            //                dispatch_after(delayTime, dispatch_get_main_queue()) {
            let VC = self.storyboard?.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
            VC.comingFirstTime = true
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "IPShowcard")
            self.navigationController?.pushViewController(VC, animated: false)
            //                self.performSegueWithIdentifier("LaunchToHome", sender: self)
            //                }
            
        }else{
            let VC = self.storyboard?.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
            
            navigationController?.pushViewController(VC, animated: false)
        }
    }
}
