//
//  ShareViewController.swift
//  InPerson
//
//  Created by CB Macmini_3 on 04/01/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController,UINavigationControllerDelegate {
    
    
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var imageShare: UIImageView!
    @IBOutlet var btnShare: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.delegate = self
        self.btnClose.hidden = false
        animateAppear()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.btnClose.hidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func actionBtnShare(sender: UIButton) {
        
        let  activityController = UIActivityViewController(activityItems: ["Thought you might like this app for business networking at airports-http://www.codebrew.com"], applicationActivities: [])
        activityController.setValue("InPerson Mobile App", forKey: "subject")
        
        let arrExcludedTypes = [UIActivityTypeAirDrop,UIActivityTypePrint,
            UIActivityTypeAssignToContact,
            UIActivityTypeSaveToCameraRoll,
            UIActivityTypeAddToReadingList,
            UIActivityTypePostToFlickr,
            UIActivityTypePostToVimeo]
        
        activityController.excludedActivityTypes = arrExcludedTypes
        
        self.presentViewController(activityController, animated: true, completion: nil)
        
    }
    @IBAction func actionBtnClose(sender: UIButton) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func animateAppear(){
        self.btnClose.alpha = 1.0
        self.btnShare.alpha = 1.0
        self.imageShare.alpha = 1.0
        self.viewContainer.alpha = 1.0
    }
    
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == .Pop {
            
            let ping = LoginTransitionAnimatorDismiss()
            return ping;
        }else{
            return nil;
        }
    }
}
