//
//  RKViewController.swift
//  InPerson
//
//  Created by CB Macmini_3 on 17/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import UIKit

class RKViewController: UIViewController {

    var loader : Loader!
    
    
    func showLoader(){
       
        loader = Loader(frame: view.frame)
        loader.labelLoadingText.hidden = true
        view.addSubview(loader)
        view.bringSubviewToFront(loader)
        loader.startAnimating()
    }
    
    func showLoaderWithText(){
        
        loader = Loader(frame: view.frame)
        loader.labelLoadingText.hidden = false
        loader.constraintCenterVertically.constant = -40
        loader.updateConstraints()
        view.addSubview(loader)
        view.bringSubviewToFront(loader)
        loader.startAnimating()
    }
    func showLoader(text : String){
        
        loader = Loader(frame: view.frame)
        loader.labelLoadingText.hidden = false
        loader.labelLoadingText.text = "Loading..."
        loader.constraintCenterVertically.constant = -40
        loader.updateConstraints()
        view.addSubview(loader)
        view.bringSubviewToFront(loader)
        loader.startAnimating()
    }
    
    func hideLoader(){
        guard let _ = loader else { return }
        loader.stopAnimating()
        loader.removeFromSuperview()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("handlePush:"), name: "IPPush", object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func showNativeAlert(message : String){
        
        let controller = UIAlertController(title: nil, message: message , preferredStyle: UIAlertControllerStyle.Alert)
        let yes = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in }
        controller.addAction(yes)
        self.presentViewController(controller, animated: true, completion: nil)
    }
    func showNativeAlert(message : String,buttonTitle : String){
        
        let controller = UIAlertController(title: nil, message: message , preferredStyle: UIAlertControllerStyle.Alert)
        let yes = UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.Default) { (action) -> Void in }
        controller.addAction(yes)
        self.presentViewController(controller, animated: true, completion: nil)
    }
    func showNativeAlert(title : String ,message : String,cancel: () -> (), confirm: () -> () ){
        
        let controller = UIAlertController(title: title, message: message , preferredStyle: UIAlertControllerStyle.Alert)
        let yes = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) { (action) -> Void in
            cancel()
        }
        let confirm = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in
            confirm()
        }
        controller.addAction(yes)
        controller.addAction(confirm)
        self.presentViewController(controller, animated: true, completion: nil)
    }
    func showNativeAlert(title : String ,message : String,buttonTitle : String,cancel: () -> (), confirm: () -> () ){
        
        let controller = UIAlertController(title: title, message: message , preferredStyle: UIAlertControllerStyle.Alert)
        let yes = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) { (action) -> Void in
            cancel()
        }
        let confirm = UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.Default) { (action) -> Void in
            confirm()
        }
        controller.addAction(yes)
        controller.addAction(confirm)
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    func logout(){
        
        guard let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate else { return }
        appDelegate.logout()
    }
    
    //Push Notification Handler
    
    func handlePush(notification : NSNotification){
        guard let pi = notification.userInfo else { return }
        let push = Push(pushInfo: pi)
        let dict = pi as NSDictionary
        if push.type == "0"{ setupMessageBanner() }
        if UIApplication.sharedApplication().applicationState == .Active {
            self.setupBadge(push.badgeCount ?? "0")
            HDNotificationView.showNotificationViewWithImage(UIImage(named: "push.png"), title: "InPerson", message: dict.valueForKeyPath("aps.alert.body") as! String , isAutoHide: true, onTouch: { () -> Void in
                HDNotificationView.hideNotificationView()
                self.pushNotificationHandler(push)
               
            })
        }else {
            pushNotificationHandler(push)
        }
    }
    
    func pushNotificationHandler(push : Push){
        
        if push.type == "1" {
            
            guard let VC = self.storyboard?.instantiateViewControllerWithIdentifier("ChatViewController") as? ChatViewController else { return }
            VC.userId2 = push.senderId
            
            VC.myImageUrl = push.senderProfilePic
            VC.otherUserName = push.senderName
            VC.placeholderHidden = true
            self.navigationController?.pushViewController(VC, animated: true)
        }else{
            guard let VC = self.storyboard?.instantiateViewControllerWithIdentifier("MessageListingViewController") as? MessageListingViewController else { return }
            self.navigationController?.pushViewController(VC, animated: true)
        }
        NSUserDefaults.standardUserDefaults().removeObjectForKey("IPPush")
    }
    
    func setupBadge(badgeCount : String){
        
    }
    
    func setupMessageBanner(){
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "IPMessageBanner")
    }
}
