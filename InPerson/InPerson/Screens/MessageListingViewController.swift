//
//  MessageListingViewController.swift
//  InPerson
//
//  Created by CB Macmini_3 on 08/01/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

class MessageListingViewController: RKViewController {

    @IBOutlet var imagePaperPlane: UIImageView!
    @IBOutlet var labelPlaceholder: UILabel!
    @IBOutlet var constraintHeaderLineHeight: NSLayoutConstraint!{
        didSet{
            constraintHeaderLineHeight.constant = 0.5
        }
    }
    @IBOutlet var tableView: UITableView!
    var indexPath : NSIndexPath?
    
    var tableDataSource : MessageDataSource?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        getChatListing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        hideLoader()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let VC = segue.destinationViewController as? ChatViewController else { return }
        guard let chatArr = self.tableDataSource?.arrChats ,reqArr = self.tableDataSource?.arrRequests,index = self.indexPath,reqAccepted = sender as? Bool else { return }
        if !reqAccepted {
            VC.userId2 = chatArr.first?.chatSentTo
            VC.myImageUrl = chatArr.first?.chatImage
            VC.otherUserName = chatArr.first?.chatName
            VC.placeholderHidden = chatArr.first?.chatMessage == "" ? false : true
            
        }else {
            VC.userId2 = chatArr[index.row - reqArr.count].chatSentTo
            VC.myImageUrl = chatArr[index.row - reqArr.count].chatImage
            VC.otherUserName = chatArr[index.row - reqArr.count].chatName
            VC.placeholderHidden = chatArr[index.row - reqArr.count].chatMessage == "" ? false : true
        }
//       //        VC.array_messages = ChatDBManager().retrieveSavedLoggedInMessages(VC.userId2!)
    }
    func getChatListing(){
        
        guard let userId = RKUserSingleton.sharedInstance.loggedInUser?.userId, token = RKUserSingleton.sharedInstance.loggedInUser?.userToken else { return }
        let timezone = NSTimeZone.localTimeZone()
        let zone = timezone.name
        User.getAllMessagelisting(userId, token: token, zone: zone, success: { (requests, chats,adminMessage) -> () in
            
            self.messageWebHandler(requests, chats: chats,adminMessage: adminMessage)
            }, failure: { (error) -> () in
                
                super.showNativeAlert(error)
            }) { () -> () in
                
                super.logout()
        }
    }
    
    func messageWebHandler(requests : [Request], chats : [Chat],adminMessage : Chat){
        
        tableDataSource = MessageDataSource(table: self.tableView, req: requests, chats: chats,selfVC: self,adminMessage: adminMessage)
        self.tableView.delegate = tableDataSource
        self.tableView.dataSource = tableDataSource
        
        if let _ = adminMessage.chatId where chats.count + requests.count == 0 {
            
            labelPlaceholder.hidden = true
            imagePaperPlane.hidden = false
        }else if let _ = adminMessage.chatId where chats.count + requests.count == 0 {
            labelPlaceholder.hidden = false
            imagePaperPlane.hidden = false
        }else if let _ = adminMessage.chatId where chats.count + requests.count == 1{
            labelPlaceholder.hidden = true
            imagePaperPlane.hidden = true
        }else if let _ = adminMessage.chatId where chats.count + requests.count > 1{
            labelPlaceholder.hidden = true
            imagePaperPlane.hidden = true
        }else if chats.count + requests.count == 0{
            labelPlaceholder.hidden = false
            imagePaperPlane.hidden = false
        }else if chats.count + requests.count == 1{
            labelPlaceholder.hidden = true
            imagePaperPlane.hidden = false
        }else{
            labelPlaceholder.hidden = true
            imagePaperPlane.hidden = true
        }
    }
    
    override func pushNotificationHandler(push: Push) {
        
        getChatListing()
    }
    

    @IBAction func actionBtnClose(sender: UIButton) {
        
//        self.performSegueWithIdentifier("BackToHome", sender: self)
        navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func unwindToMessages(segue: UIStoryboardSegue) {
        
    }
}
