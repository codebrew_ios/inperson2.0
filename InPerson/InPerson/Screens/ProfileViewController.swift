//
//  ProfileViewController.swift
//  InPerson
//
//  Created by CB Macmini_3 on 18/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation

class ProfileViewController: RKViewController {

    
    var navController : UINavigationController?
    @IBOutlet var labelIndicator: UILabel!{
        didSet{
            labelIndicator.layer.cornerRadius = 4.0
            labelIndicator.clipsToBounds = true
        }
    }
    @IBOutlet var labelPlace: UILabel!
    @IBOutlet var labelJob: UILabel!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var imageviewUser: UIImageView!
    @IBOutlet var btnViewLinkedInProfile: UIButton!{
        didSet{
            btnViewLinkedInProfile.layer.cornerRadius = 2
            btnViewLinkedInProfile.layer.borderColor = UIColor(red: 1, green: 1.0, blue: 1, alpha: 0.5).CGColor
            btnViewLinkedInProfile.layer.borderWidth = 1.0

        }
    }
    @IBOutlet var switches: [UISwitch]!{
        didSet{
            for aswitch in switches {
                aswitch.backgroundColor = UIColor(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1.0)
                aswitch.layer.cornerRadius = 16.0
                aswitch.clipsToBounds = true
            }
        }
    }
    
//    func setUpNavigationButtons(){
//        
//        let btn = UIButton(frame: CGRectMake(self.view.frame.size.width-50, 0, 64, 40))
//        btn.setTitleColor(appColor, forState: .Normal)
//        btn.setTitle("Log Out", forState: .Normal)
//        btn.titleLabel?.adjustsFontSizeToFitWidth = true
//        btn.titleLabel?.font = UIFont.systemFontOfSize(20.0)
//        btn.addTarget(self, action: Selector("actionBtnLogOut:"), forControlEvents: .TouchUpInside)
//        let rightItem = UIBarButtonItem(customView: btn)
//        self.navigationItem.rightBarButtonItem = rightItem
//        
//        let barItem = UIBarButtonItem(image: UIImage(named: "ic_close.png"), landscapeImagePhone: nil, style: .Plain, target: self, action: Selector("actionBtnClose:"))
//        self.navigationItem.leftBarButtonItem = barItem
//    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        getUserProfile()
//        setUpNavigationButtons()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let _ = segue.destinationViewController as? UINavigationController else { return }
        
        if segue.identifier == "ProfileToSignIn" {
        
            RKUserSingleton.sharedInstance.loggedInUser = nil
        }
    }
    
    override func canPerformUnwindSegueAction(action: Selector, fromViewController: UIViewController, withSender sender: AnyObject) -> Bool {
        
        return true
    }
    
    func getUserProfile(){
        
        guard let user = RKUserSingleton.sharedInstance.loggedInUser else { return }
            
            updateUI(user)
      
        User.getUserProfile(user.userId!, token: user.userToken!, success: { (response,hidden,push,loc) -> () in
            
            self.handleProfileData(response,hidden: hidden,push: push,loc: loc)
            }, failure: { (error) -> () in
                
                super.showNativeAlert(error)
            }) { () -> () in
                
             super.logout()
        }
    }
    
    func handleProfileData(user : User , hidden : Bool? , push : Bool?,loc : Bool?){
        
        guard let _ = hidden , _ = push , _ = loc else { return }
        updateUI(user)
        switches[0].setOn(hidden!, animated: false)
        switches[1].setOn(push!, animated: false)
        switches[2].setOn(loc!, animated: false)
    }
    func updateUI(user : User){
        guard let userName = user.userName,hide = user.userHide,push = user.userPush,loc = user.userLoc else { return }
        labelPlace.text = user.userCity ?? ""
        labelJob.text = user.userJob ?? ""
        labelUserName.text = userName
        let imageUrl = user.userImageName!
        imageviewUser.sd_setImageWithURL(NSURL(string: imageUrl))
        
        switches[0].setOn(hide.toBool()!, animated: true)
        switches[1].setOn(push.toBool()!, animated: true)
        switches[2].setOn(loc.toBool()!, animated: true)
    }
    
    @IBAction func actionBtnClose(sender: UIButton) {
        
//         dismissViewControllerAnimated(true, completion: nil)
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func actionBtnViewLinkedinProfile(sender: UIButton) {
        
        guard let VC = self.storyboard?.instantiateViewControllerWithIdentifier("LinkedinProfileViewController") as? LinkedinProfileViewController,profileUrl = RKUserSingleton.sharedInstance.loggedInUser?.userProfileUrl,name = RKUserSingleton.sharedInstance.loggedInUser?.userName else { return }
        VC.linkedinProfileUrl = profileUrl
        VC.userName = name
        self.presentViewController(VC, animated: true, completion: nil)
    }
    @IBAction func actionBtnLogout(sender: UIButton) {
        
        super.showNativeAlert("Are you sure you want to log out?", message: "", buttonTitle: "Yes", cancel: { () -> () in
            
            
            }) { () -> () in
                
                guard let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate else { return }
                appDelegate.logoutWithoutAlert()
        }
    }
    
    @IBAction func switchChanged(sender: UISwitch) {
//        showLoader()
        
        guard let h = RKUserSingleton.sharedInstance.loggedInUser?.userHide,p = RKUserSingleton.sharedInstance.loggedInUser?.userPush, l = RKUserSingleton.sharedInstance.loggedInUser?.userLoc else { return }
        let hide,push,loc : String
        
        if sender.tag == 1 {
            hide = sender.on ? "1" : "0"
            push = p
            loc = l
        }else if sender.tag == 2{
            
            hide = h
            push = sender.on ? "1" : "0"
            loc = l
        }else{
            
            hide = h
            push = p
            loc = sender.on ? "1" : "0"
        }
        guard let user = RKUserSingleton.sharedInstance.loggedInUser else { return }
        
        User.changeUserSettings(user.userId!, token: user.userToken!, hide: hide, push: push, loc: loc, success: { (User) -> () in
            
            }, failure: { (error) -> () in
                super.showNativeAlert(error)
                super.hideLoader()
            }) { () -> () in
                
                super.logout()
        }
    }
}
