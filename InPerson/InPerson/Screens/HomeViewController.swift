//
//  HomeViewController.swift
//  InPerson
//
//  Created by CB Macmini_3 on 24/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import Foundation
import CoreLocation

class HomeViewController: RKViewController,UITableViewDelegate,UIViewControllerTransitioningDelegate,MDDelegate,CLLocationManagerDelegate,SearchDelegate,UINavigationControllerDelegate,HomeTableHeaderViewDelegate,ExpandingTransitionPresentingViewController {
    
    
    var comingFirstTime : Bool = false
    @IBOutlet var btnAnimator: UIButton!
    let loginTransitionDelegate = LoginTransitionAnimator()
    var timer : NSTimer?
    let badgeView = GIBadgeView()
    var selectedIndexPath : NSIndexPath?
    var locationManager = CLLocationManager()
    var location : Location? = Location()
    
    let customPresentAnimationController = CustomPresentAnimationController()
    let customDismissAnimationController = CustomDismissAnimationController()
    
    var headerView = HomeTableHeaderView()
    
    @IBOutlet var btnChat: UIButton!
    
    var tableDataSource : HomeTableDataSource = HomeTableDataSource()
    
    //MARK: - constraints
    
    //MARK: - IBOutlets
    @IBOutlet var btnSearchTop: UIButton!{
        didSet{
            btnSearchTop.layer.cornerRadius = 3.0
            btnSearchTop.clipsToBounds = true
        }
    }
    
    @IBOutlet var tableViewListings: UITableView!
    @IBOutlet var labelPerson: UILabel!{
        didSet{
            labelPerson.font = UIFont(name: "OpenSans", size: 18.0)
        }
    }
    @IBOutlet var viewNavigationBar: UIView!
    
    var users : [User] = []
    
    var firstTime : Bool = true
    
    //MARK: - View Hierarchy
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpBadgeView()
        
        labelPerson.hidden = false
        setupHome()
        guard let p = NSUserDefaults.standardUserDefaults().objectForKey("IPPush") as? NSDictionary else { return }
        let push = Push(pushInfo: p)
        pushNotificationHandler(push)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.delegate = self
        //        self.allusersWebhandler([])
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.checkAirport()
        }
        
    }
    
    func setupHome(){
        //                guard  let statusBar = UIApplication.sharedApplication().valueForKey("statusBarWindow")?.valueForKey("statusBar") as? UIView else {
        //                    return
        //                }
        //        statusBar.backgroundColor = UIColor.blackColor()
        self.headerView = HomeTableHeaderView(frame: CGRectMake(0,0,UIScreen.mainScreen().bounds.width,300))
        self.headerView.delegate = self
        tableDataSource.initWithTableView(self.tableViewListings, view: self.view,users:[RKUserSingleton.sharedInstance.loggedInUser ?? User()] ,headerView: self.headerView,navBar: viewNavigationBar,btnST: btnSearchTop,labelp: labelPerson)
        self.tableDataSource.homeVC = self
        self.tableViewListings.delegate = tableDataSource
        self.tableViewListings.dataSource = tableDataSource
        self.tableViewListings.contentInset = UIEdgeInsetsMake(300, 0, 0, 0)
        //        self.tableViewListings.contentOffset = CGPointMake(0, 300)
        updateHeaderView()
        timer = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: Selector("bounce:"), userInfo: nil, repeats: true)
        startTrackingUser()
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return false
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .Default
    }
    
    func updateHeaderView(){
        
        var headerRect = CGRect(x: 0, y: -300, width: UIScreen.mainScreen().bounds.width, height: 300)
        
        if tableViewListings.contentOffset.y > -310 {
            
            headerRect.origin.y = tableViewListings.contentOffset.y
            headerRect.size.height = -tableViewListings.contentOffset.y
        }
        headerView.frame = headerRect
    }
    
    
    //MARK: - Button Actions
    @IBAction func actionBtnShare(sender: UIButton) {
        guard let VC = self.storyboard?.instantiateViewControllerWithIdentifier("ShareViewController") as? ShareViewController else { return }
        self.navigationController?.pushViewController(VC, animated: true)
    }
    @IBAction func actiontBtnSearch(sender: UIButton) {
//        guard let VC = self.storyboard?.instantiateViewControllerWithIdentifier("SearchAirportViewController") as? SearchAirportViewController else { return }
//        VC.delegate = self
//        self.navigationController?.pushViewController(VC, animated: true)
        
                self.performSegueWithIdentifier("HomeToSearch", sender: self)
    }
    
    @IBAction func actionBtnContacts(sender: UIButton) {
           guard let VC = self.storyboard?.instantiateViewControllerWithIdentifier("ProfileViewController") as? ProfileViewController else { return }
        //        ChatDBManager.deleteAllFromEntityName(entityName: "Chat")
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
         
            self.navigationController?.pushViewController(VC, animated: true)
//            self.performSegueWithIdentifier("HomeToProfile", sender: self)
        }
    }
    @IBAction func actionBtnChat(sender: UIButton) {
        let type: UIUserNotificationType = [UIUserNotificationType.Badge, UIUserNotificationType.Alert, UIUserNotificationType.Sound]
        let setting = UIUserNotificationSettings(forTypes: type, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(setting)
        UIApplication.sharedApplication().registerForRemoteNotifications()
//        self.performSegueWithIdentifier("HomeToMessages", sender: self)
//        badgeView.badgeValue = 0
        guard let VC = self.storyboard?.instantiateViewControllerWithIdentifier("MessageListingViewController") as? MessageListingViewController else { return }
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func setUpBadgeView(){
//        UIApplication.sharedApplication().statusBarHidden = true
//        self.setNeedsStatusBarAppearanceUpdate()
        btnChat.addSubview(badgeView)
        badgeView.topOffset = 10
        badgeView.rightOffset = 10
    }
    
    func bounce(timer : NSTimer){
        updateuserHere()
    }
    
    func updateuserHere(){
        guard let user = RKUserSingleton.sharedInstance.loggedInUser,lat = RKLocationManager.sharedInstance.currentLocation?.current_lat , lng = RKLocationManager.sharedInstance.currentLocation?.current_lng,airID = RKUserSingleton.sharedInstance.selectedAirport?.airportId else {
            self.timer?.invalidate()
            self.timer =  nil
            return }
        
        User.addUserToAirport(user.userId!, airId: airID, lat: lat, lng: lng, token: user.userToken!, success: { (stop) -> () in
            
            if stop { self.timer?.invalidate() }
            }, failure: { (error) -> () in
                
                
            }) { () -> () in
                
                self.timer?.invalidate()
                super.logout()
        }
    }
    
    
    func checkAirport(){
        
        if let currentAirport = RKUserSingleton.sharedInstance.selectedAirport,_ = currentAirport.airportId {
            
            if let ac = currentAirport.airportCode where ac != "" {
                btnSearchTop.setTitle(currentAirport.airportName! + " (" + currentAirport.airportCode! + ")", forState: .Normal)
                self.headerView.btnAirportName.setTitle(currentAirport.airportName! + " (" + currentAirport.airportCode! + ")", forState: .Normal)
            }else{
                
                btnSearchTop.setTitle(currentAirport.airportName!, forState: .Normal)
            }
            
            guard let userId = RKUserSingleton.sharedInstance.loggedInUser?.userId,lat = RKLocationManager.sharedInstance.currentLocation?.current_lat , lng = RKLocationManager.sharedInstance.currentLocation?.current_lng,token = RKUserSingleton.sharedInstance.loggedInUser?.userToken else {
                
                return }
            if comingFirstTime {
                comingFirstTime = false
                
                getUsersWithAirport(userId, token: token, lat: lat, lng: lng) { () -> () in
                    let animation = CAKeyframeAnimation.dockBounceAnimationWithIconHeight(self.headerView.imageArrow.frame.size.height)
                    self.headerView.imageArrow.layer.addAnimation(animation, forKey: "jumping")
                }
                
            }else {
                if let ac = currentAirport.airportCode where ac != "" {
                    //                    self.btnSearchTop.setTitle(currentAirport.airportName! + " (" + currentAirport.airportCode! + ")", forState: .Normal)
                    self.headerView.btnAirportName.setTitle(currentAirport.airportName! + " (" + currentAirport.airportCode! + ")", forState: .Normal)
                }else{
                    
                    self.btnSearchTop.setTitle(currentAirport.airportName!, forState: .Normal)
                    self.headerView.btnAirportName.setTitle(currentAirport.airportName!, forState: .Normal)
                }
                
                getUsersWithoutAirport(userId, airId: currentAirport.airportId! , token: token, lat: lat, lng: lng)
            }
            
        }else{
            
            
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                super.hideLoader()
                self.locationUpdated(RKLocationManager.sharedInstance.currentLocation)
            }
            
        }
    }
    
//    func logOut(){
//        
//        RKUserSingleton.sharedInstance.loggedInUser = nil
//        self.navigationController?.popToRootViewControllerAnimated(true)
//    }
    
    func locationUpdated(location: Location?) {
        guard let loc = location else { return }
        
        if let _ = RKUserSingleton.sharedInstance.selectedAirport?.airportId {
            
        }else {
            
            showLoaderWithText()
            if let _ = RKUserSingleton.sharedInstance.loggedInUser?.userId {}else {
                //                super.logout()
            }
            guard let user = RKUserSingleton.sharedInstance.loggedInUser,id = user.userId,token = user.userToken,lat = loc.current_lat , lng = loc.current_lng else {
                
                super.hideLoader()
                return }
            getUsersWithAirport(id, token: token, lat: lat, lng: lng) { () -> () in
                
            }
        }
    }
    
    
    
//    let transition = BubbleTransition()
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "circRev" {
            
            let controller = segue.destinationViewController
            controller.transitioningDelegate = self
            controller.modalPresentationStyle = .Custom
            
        }else if segue.identifier == "HomeToMessages" {
            
            let controller = segue.destinationViewController
            controller.transitioningDelegate = self
        }else if segue.identifier == "HomeToProfile" {
            
            guard let controller = segue.destinationViewController as? ProfileViewController else { return }
            controller.navController = self.navigationController
            controller.transitioningDelegate = self
        }else if segue.identifier == "HomeToSearch" {
            
            guard let controller = segue.destinationViewController as? SearchAirportViewController else { return }
            controller.delegate = self
            controller.transitioningDelegate = self
        }
    }
    
    // MARK: UIViewControllerTransitioningDelegate
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        //        print(presented.classForCoder.description())
        if presented.classForCoder.description() == "InPerson.ShareViewController" {
//            transition.transitionMode = .Present
//            transition.startingPoint = btnAnimator.center
//            transition.bubbleColor = appColor
//            transition.duration = 0.5
            return LoginTransitionAnimator()
        }
        return customPresentAnimationController
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if dismissed.classForCoder.description() == "InPerson.ShareViewController" {
//            transition.transitionMode = .Dismiss
//            transition.startingPoint = btnAnimator.center
//            transition.bubbleColor = appColor
//            transition.duration = 0.5
            return LoginTransitionAnimatorDismiss()
        }
        
        return customDismissAnimationController
    }

    
    func showLinkedinProfile(indexpath: NSIndexPath) {
        
        guard let user = self.tableDataSource.items?[indexpath.row],otherId = user.userId,oProfileURL = user.userProfileUrl else { return }
        guard let VC2 = self.storyboard?.instantiateViewControllerWithIdentifier("LinkedinProfileViewController") as? LinkedinProfileViewController,me = RKUserSingleton.sharedInstance.loggedInUser,profileUrl = me.userProfileUrl,name = me.userName,id = me.userId else { return }
        
        if id == otherId {
            VC2.linkedinProfileUrl = profileUrl
            VC2.userName = name
        }else {
            
            VC2.linkedinProfileUrl = oProfileURL
            VC2.userName = user.userName
        }
        
        
        self.presentViewController(VC2, animated: true, completion: nil)
    }
    
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
    }
    
    //MARK: - Location Manager Delegate
    
    func startTrackingUser(){
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.startUpdatingLocation()
        }
        RKLocationManager.sharedInstance.startTrackingUser()
    }
    
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print(error.localizedDescription)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = (manager.location?.coordinate) ?? CLLocationCoordinate2D()
        if let _ = self.location?.current_lat {
            self.location?.current_lat = "\(locValue.latitude)"
            self.location?.current_lng = "\(locValue.longitude)"
            RKLocationManager.sharedInstance.currentLocation = self.location
        }else{
            self.location?.current_lat = "\(locValue.latitude)"
            self.location?.current_lng = "\(locValue.longitude)"
            self.locationUpdated(self.location)
        }
    }
    
    func selectedAirport() {
        
        guard let airport = RKUserSingleton.sharedInstance.selectedAirport, user = RKUserSingleton.sharedInstance.loggedInUser,lat = RKLocationManager.sharedInstance.currentLocation?.current_lat , lng = RKLocationManager.sharedInstance.currentLocation?.current_lng else {
            
            return }
        
        guard let airId = airport.airportId, airName = airport.airportName,token = user.userToken,userId = user.userId else { return }
        self.tableViewListings.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: .Top, animated: true)
        super.showLoader("Loading Professionals at " + airName)
        
        getUsersWithoutAirport(userId, airId: airId, token: token, lat: lat, lng: lng)
        
    }
    //MARK: - NavigationController delegate
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    
        if let _ = toVC as? ShareViewController where operation == .Push {
            
            let ping = LoginTransitionAnimator()
            return ping;
        }else if let _ = toVC as? SearchAirportViewController where operation == .Push{
            return ExpandingCellTransition()
        }else{
            return nil
        }
    }
    
    func searchAirport() {
        
        self.performSegueWithIdentifier("HomeToSearch", sender: self)
    }
    
    
    func relocateUser() {
        
        showLoaderWithText()
        if let _ = RKUserSingleton.sharedInstance.loggedInUser?.userId {}else {
            //            super.logout()
        }
        
        guard let user = RKUserSingleton.sharedInstance.loggedInUser, loc = RKLocationManager.sharedInstance.currentLocation,id = user.userId,token = user.userToken,lat = loc.current_lat , lng = loc.current_lng else {
            
            super.hideLoader()
            return }
        
        getUsersWithAirport(id, token: token, lat: lat, lng: lng) { () -> () in
            
        }
    }
    
    //MARK : API Calls
    
    func getUsersWithAirport(id : String ,token : String,lat : String,lng : String, success: () -> () ){
        
        User.getUsersFirstTime(id, airId: "", token: token, lat: lat, lng: lng, success: { (users,cards,unreadCount,showMessageBanner) -> () in
            guard let currentAirport = RKUserSingleton.sharedInstance.selectedAirport else { return }
            if let ac = currentAirport.airportCode where ac != "" {
                self.btnSearchTop.setTitle(currentAirport.airportName! + " (" + currentAirport.airportCode! + ")", forState: .Normal)
                self.headerView.btnAirportName.setTitle(currentAirport.airportName! + " (" + currentAirport.airportCode! + ")", forState: .Normal)
            }else{
                
                self.btnSearchTop.setTitle(currentAirport.airportName!, forState: .Normal)
            }
            success()
            super.hideLoader()
            self.allusersWebhandler(users,badgeCount: unreadCount,showMessageBanner: showMessageBanner,cards: cards)
            }, failure: { (error) -> () in
                
                super.hideLoader()
            }, tokenExpired: { () -> () in
                super.hideLoader()
                super.logout()
        })
        
    }
    
    func getUsersWithoutAirport(userId : String, airId: String, token: String, lat: String,lng: String){
        
        User.getUsers(userId, airId: airId, token: token, lat: lat,lng: lng, success: { (users,cards,unreadCount,showMessageBanner) -> () in
            super.hideLoader()
            if users.count != 0 {
                let animation = CAKeyframeAnimation.dockBounceAnimationWithIconHeight(self.headerView.imageArrow.frame.size.height)
                self.headerView.imageArrow.layer.addAnimation(animation, forKey: "jumping")
            }
            self.allusersWebhandler(users,badgeCount: unreadCount,showMessageBanner: showMessageBanner,cards: cards)
            }, failure: { (error) -> () in
                super.hideLoader()
                super.showNativeAlert(error)
            }, tokenExpired: { () -> () in
                super.hideLoader()
                super.logout()
        })
    }
    
    func allusersWebhandler(users : [User],badgeCount : String,showMessageBanner : String,cards : [ContentCard]){
        badgeView.badgeValue = Int(badgeCount) ?? 0
        guard let showScene = NSUserDefaults.standardUserDefaults().valueForKey("IPShowcard") as? Bool else { return  }
        
        if showScene {
            self.tableDataSource.items = [User(),RKUserSingleton.sharedInstance.loggedInUser!]
        }else{
            self.tableDataSource.items = [RKUserSingleton.sharedInstance.loggedInUser!]
        }
        let user = User()
        user.userName = "messageBanner"
        if NSUserDefaults.standardUserDefaults().valueForKey("IPMessageBanner") as! Bool && self.tableDataSource.items?.first?.userName != "messageBanner"{
            
            self.tableDataSource.items?.insert(user, atIndex: 0)
            
        }else if self.tableDataSource.items?.first?.userName == "messageBanner"{
            
            self.tableDataSource.items?.removeFirst()
        }
        
        guard let _ = self.tableDataSource.items else { return }
        self.tableDataSource.items! += users
        self.tableDataSource.contentCards = cards
        self.tableDataSource.delegate = self
        self.tableViewListings.reloadData()
        updateTableFooter(users)
    }
    
    
    func updateTableFooter(users : [User]){
        if users.count == 0 {
            if DeviceType.IS_IPHONE_5{
                
                self.tableViewListings?.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.size.width, height: 320))
            }else if DeviceType.IS_IPHONE_6{
                
                self.tableViewListings?.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.size.width, height:420))
            }else if DeviceType.IS_IPHONE_6P{
                
                self.tableViewListings?.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.size.width, height: 488))
            }else{
                
            }
        }
    }
    override func setupBadge(badgeCount: String) {
        badgeView.badgeValue = Int(badgeCount) ?? 0
    }
    
    
    //MARK: - Expanding Cell Animation
    func expandingTransitionTargetViewForTransition(transition: ExpandingCellTransition) -> UIView! {
        if let indexPath = selectedIndexPath {
            return tableViewListings.cellForRowAtIndexPath(indexPath)
        }
        else {
            return nil
        }
    }
}
