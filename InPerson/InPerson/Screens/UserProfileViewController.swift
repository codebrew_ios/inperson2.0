//
//  UserProfileViewController.swift
//  InPerson
//
//  Created by Apple on 17/02/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit
protocol ProfilePopUpDelegate{
    
    func profilePopUpDismissed(userName : String,profileUrl : String)
}

class UserProfileViewController: UIViewController {

    var delegate : ProfilePopUpDelegate?
    @IBOutlet var labelHere: UILabel!{
        didSet{
            labelHere.layer.cornerRadius = 4.0
            labelHere.clipsToBounds = true
        }
    }
    @IBOutlet var btnViewLinkedinProfile: UIButton!{
        didSet{
            btnViewLinkedinProfile.setTitle("View LinkedIn Profile", forState: .Normal)
            btnViewLinkedinProfile.layer.borderColor = UIColor(hex: 0xE6E6E8).CGColor
            btnViewLinkedinProfile.layer.cornerRadius = 3.0
            btnViewLinkedinProfile.layer.borderWidth = 1.0
        }
    }
    @IBOutlet var labelLocation: UILabel!
    @IBOutlet var labelDescription: UILabel!
    @IBOutlet var labelUserName: UILabel!
    @IBOutlet var imageViewUser: UIImageView!
    
    
    var currentUser : User?
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        updateUI(currentUser)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    func updateUI(user : User?){
        guard let userName = user?.userName else { return }
        labelLocation.text = user?.userCity ?? ""
        labelDescription.text = user?.userJob ?? ""
        labelUserName.text = userName
        let imageUrl = user!.userImageName!
        imageViewUser.sd_setImageWithURL(NSURL(string: imageUrl))
        
        guard let here = user?.userHere else { return }
        labelHere.text = "" + here + "      "
        if here != "Here"{ labelHere.backgroundColor = UIColor(hex: 0x8F8F8F) }
    }

    
    @IBAction func actionBtnViewprofile(sender: AnyObject) {
        
        self.dismissPopUpViewControllerWithcompletion { () -> Void in
            
//            guard let profileUrl = self.currentUser?.userProfileUrl,name = self.currentUser?.userName else { return }
//            self.delegate?.profilePopUpDismissed(name, profileUrl: profileUrl)
            
        }
    }
}
