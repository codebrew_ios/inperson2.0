//
//  SearchAirportViewController.swift
//  InPerson
//
//  Created by CB Macmini_3 on 31/12/15.
//  Copyright © 2015 Rico. All rights reserved.
//

import UIKit

protocol SearchDelegate {
    
    func selectedAirport()
}

class SearchAirportViewController: RKViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var labelPlaceHolder: UILabel!
    var delegate : SearchDelegate?
    var arrAirport : [Airport]?
    
    
    @IBOutlet var tfSearch: UITextField!
    @IBOutlet var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView(frame: CGRectZero)
        }
    }
    @IBOutlet var viewSearch: UIView!{
        didSet{
            viewSearch.layer.cornerRadius = 4.0
            viewSearch.clipsToBounds = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        getNearestAirports()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func getNearestAirports(){
        
        guard let user = RKUserSingleton.sharedInstance.loggedInUser , lat = RKLocationManager.sharedInstance.currentLocation?.current_lat , lng = RKLocationManager.sharedInstance.currentLocation?.current_lng else {
            showNativeAlert("Failed to Fetch Location")
            return
        }
        labelPlaceHolder.hidden = false
        User.getNearestAirports(user.userId!, token: user.userToken!, lat: lat, lng: lng, change: "1", success: { (airports) -> () in
            
            self.arrAirport = airports
            self.labelPlaceHolder.hidden = true
            self.tableView.reloadData()
            }) { (error,tokenExpired) -> () in
                super.hideLoader()
                
                if tokenExpired{
                    self.handleTokenExpire()
                }else{
                    super.showNativeAlert(error)
                    self.labelPlaceHolder.hidden = true
                }
        }
    }
    
    func handleTokenExpire(){
//        dispatch_async(dispatch_get_main_queue()) {
//            self.performSegueWithIdentifier("SearchToSignIn", sender: self)
//        }
        RKUserSingleton.sharedInstance.loggedInUser = nil
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let _ = self.arrAirport {
            return arrAirport!.count
        }else{
            return 0
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCellWithIdentifier("SearchCell") as? SearchCell else { return UITableViewCell() }
        
        guard let airports = arrAirport else { return UITableViewCell() }
        cell.currentAirport = airports[indexPath.row]
        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        guard let airports = arrAirport else { return }
        RKUserSingleton.sharedInstance.selectedAirport = airports[indexPath.row]
        
        self.dismissViewControllerAnimated(true) { () -> Void in
            
            self.delegate?.selectedAirport()
        }
    }
    
    //MARK: - Button Actions
    @IBAction func actionBtnClose(sender: UIButton) {
        
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func textfieldEditingChanged(sender: UITextField) {
        
        guard let _ = RKUserSingleton.sharedInstance.loggedInUser , searchKey = sender.text else {
            return
        }
        if searchKey == "" { return }
        if let array : NSArray = arrAirport as NSArray? {
         
            let predicate = NSPredicate(format: "SELF.airportName CONTAINS[cd] %@", searchKey)
            var temparr = array.filteredArrayUsingPredicate(predicate)
            for airport in array {
                guard let ap = airport as? Airport,name = ap.airportName,code = ap.airportCode else { return }
                if name.hasPrefix(searchKey) || code.hasPrefix(searchKey){
                    temparr.append(airport)
                }
            }
            
            if temparr.count > 0 {
                
                arrAirport = temparr as? [Airport]
                self.tableView.reloadData()
            }else {
                
              searchWeb(searchKey)
            }
        }
    }
    
    func searchWeb(searchKey : String){
        guard let user = RKUserSingleton.sharedInstance.loggedInUser else {
            return
        }

        Airport.searchAirport(user.userId!, token: user.userToken!, searchKey: searchKey, success: { (airports) -> () in
            
            if airports.count > 0{
                
                self.arrAirport = airports
                self.tableView.reloadData()
            }else {
                
//                super.showNativeAlert("No Results")
            }

            }, failure: { (error) -> () in
                
                super.showNativeAlert(error)
            }) { () -> () in
                
                super.logout()
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
  
    
}
