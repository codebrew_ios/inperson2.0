//
//  StringExtension.swift
//  InPerson
//
//  Created by CB Macmini_3 on 02/02/16.
//  Copyright © 2016 Rico. All rights reserved.
//

import UIKit

extension String {
    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }
}
class StringExtension: NSObject {

}
